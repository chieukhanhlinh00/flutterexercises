import 'package:flutter/material.dart';
import '../validators/LoginValidator.dart';
import '../ui/stopwatch_screen.dart';

class LoginScreen extends StatefulWidget{
  const LoginScreen({Key? key}) : super(key: key);
  static const route = '/login';
  @override
  State<StatefulWidget> createState(){
    return LoginScreenState();
  }
}

class LoginScreenState extends State<StatefulWidget> with LoginValidator{
  final formKey = GlobalKey<FormState>();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(title: const Text('Login'),
        backgroundColor: Colors.black87,),
      body: buildLoginForm(),
    );
  }

  Widget buildLoginForm(){
    return Form(
      key: formKey,
      child: Column(
        children: [
          emailField(),
          passwordField(),
          loginButton(),
        ],
      ),
    );
  }

  Widget emailField(){
    return TextFormField(
      controller: emailController,
      decoration: const InputDecoration(
        icon: Icon(Icons.person),
        labelText: 'Email address'
      ),
      validator: validateEmail,
    );
  }

  Widget passwordField(){
    return TextFormField(
      controller: passwordController,
      obscureText: true,
      decoration: const InputDecoration(
          icon: Icon(Icons.password),
          labelText: 'Password'
      ),
      validator: validatePassword,
    );
  }

  //change color login button
  Widget loginButton(){
    return ElevatedButton(
      onPressed: validate,
      child:const Text('Login'),
      style: ElevatedButton.styleFrom(
        primary: Colors.black87,
      ),

    );
  }

  void validate(){
    final form = formKey.currentState;
    if(!form!.validate()){
      return;
    }
    else {
      final email = emailController.text;
      final password = passwordController.text;

      Navigator.of(context).pushReplacementNamed(StopwatchScreen.route, arguments: email);
      setState(() {

      });
    }
  }
}