import 'package:flutter/material.dart';
import '../ui/login_screen.dart';
import '../ui/stopwatch_screen.dart';

class StopwatchApp extends StatelessWidget{
  const StopwatchApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context){
    return MaterialApp(
      routes: {
        '/': (context) => const LoginScreen(),
        '/login': (context) => const LoginScreen(),
        StopwatchScreen.route: (context) => const StopwatchScreen()
      },
      initialRoute: '/',
    );
  }
}