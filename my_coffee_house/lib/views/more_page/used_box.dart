import 'package:flutter/material.dart';
import 'package:my_coffee_house/views/more_page/detail_used_box/provision.dart';
import 'package:my_coffee_house/views/more_page/detail_used_box/songs.dart';

import '../../models/data_more.dart';
import '../login_page/login_page.dart';
import 'detail_used_box/history.dart';

class Utility extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          height: 110,
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.all(20),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(8),
            border: Border.all(color: Colors.white, width: 1),
          ),
          child: GestureDetector(
            onTap: () => Navigator.of(context)
                .push(MaterialPageRoute(builder: (_) => History())),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Image(image: AssetImage('assets/moreImg/icon1.png')),
                SizedBox(
                  height: 15,
                ),
                Text(
                  'Lịch sử đơn hàng',
                  style: TextStyle(fontSize: 18),
                )
              ],
            ),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Container(
                height: 110,
                // width: MediaQuery.of(context).size.width * 0.427,
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: Colors.white, width: 1),
                ),

                child: GestureDetector(
                  onTap: () => Navigator.of(context)
                      .push(MaterialPageRoute(builder: (_) => Song())),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Image(image: AssetImage('assets/moreImg/icon2.png')),
                      SizedBox(
                        height: 0,
                      ),
                      Text(
                        'Nhạc đang phát',
                        style: TextStyle(fontSize: 18),
                      )
                    ],
                  ),
                ),
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
              child: Container(
                height: 110,
                // width: MediaQuery.of(context).size.width * 0.427,
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: Colors.white, width: 1),
                ),

                child: GestureDetector(
                  onTap: () => Navigator.of(context)
                      .push(MaterialPageRoute(builder: (_) => Provision())),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Image(image: AssetImage('assets/moreImg/icon3.png')),
                      SizedBox(
                        height: 0,
                      ),
                      Text(
                        'Điều khoản',
                        style: TextStyle(fontSize: 18),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}

class Help extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 20),
      child: ListView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemCount: titleListHelp.length,
        scrollDirection: Axis.vertical,
        itemBuilder: (context, index) => Container(
          child: Card(
            margin: EdgeInsets.only(bottom: 1),
            elevation: 0.0,
            child: Padding(
              padding: EdgeInsets.only(left: 20),
              child: ListTile(
                onTap: () => {Navigator.of(context).pushNamed(Login.route)},
                title: Text(
                  titleListHelp[index],
                  style: TextStyle(fontSize: 18),
                ),
                leading: Icon(
                  tittleIconHelp[index],
                  color: Colors.black54,
                ),
                trailing: Icon(
                  Icons.keyboard_arrow_right_rounded,
                  color: Colors.black54,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class Account extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 20),
      child: ListView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemCount: titleListAccount.length,
        scrollDirection: Axis.vertical,
        itemBuilder: (context, index) => Container(
          child: Card(
            margin: EdgeInsets.only(bottom: 1),
            elevation: 0.0,
            child: Padding(
              padding: EdgeInsets.only(left: 20),
              child: ListTile(
                onTap: () => {Navigator.of(context).pushNamed(Login.route)},
                title: Text(
                  titleListAccount[index],
                  style: TextStyle(fontSize: 18),
                ),
                leading: Icon(
                  tittleIconAccount[index],
                  color: Colors.black54,
                ),
                trailing: Icon(
                  Icons.keyboard_arrow_right_rounded,
                  color: Colors.black54,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
