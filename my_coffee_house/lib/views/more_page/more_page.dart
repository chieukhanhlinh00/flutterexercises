import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../login_page/login_page.dart';
import '../voucher_page/tickets_voucher/ticket.dart';
import 'used_box.dart';

class MorePage extends StatefulWidget {
  static const route = '/more';
  State<MorePage> createState() => _MorePageState();
}

class _MorePageState extends State<MorePage> {
  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        title: Text(
          'Khác',
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.w600),
        ),
        actions: [
          Container(
            width: 40,
            decoration: BoxDecoration(
                border: Border.all(color: Colors.grey, width: 0.5),
                color: Colors.white,
                shape: BoxShape.circle,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.7),
                    spreadRadius: 1,
                    blurRadius: 3,
                  )
                ]),
            child: IconButton(
              color: Colors.black,
              iconSize: 20,
              icon: FaIcon(FontAwesomeIcons.ticketAlt),
              onPressed: () => Navigator.of(context)
                  .push(MaterialPageRoute(builder: (_) => Ticket())),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 8, right: 8),
            width: 40,
            decoration: BoxDecoration(
                border: Border.all(color: Colors.grey, width: 0.5),
                color: Colors.white,
                shape: BoxShape.circle,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.7),
                    spreadRadius: 1,
                    blurRadius: 3,
                  )
                ]),
            child: IconButton(
              color: Colors.black,
              iconSize: 20,
              icon: Icon(Icons.notifications),
              onPressed: () {
                Navigator.of(context).pushNamed(Login.route);
              },
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          color: Colors.grey[100],
          padding: EdgeInsets.all(30),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Tiện ích',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 20,
              ),
              Utility(),
              SizedBox(
                height: 20,
              ),
              Text(
                'Hỗ trợ',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 20,
              ),
              Help(),
              SizedBox(
                height: 20,
              ),
              Text(
                'Tài khoản',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 20,
              ),
              Account(),
            ],
          ),
        ),
      ),
    );
  }
}
