import 'package:flutter/material.dart';

class History extends StatefulWidget {
  static const route = '/history';
  @override
  _HistoryState createState() => _HistoryState();
}

class _HistoryState extends State<History> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        automaticallyImplyLeading: false,
        leadingWidth: 50,
        leading: ElevatedButton(
          onPressed: () => Navigator.of(context).pop(),
          child: Icon(Icons.keyboard_arrow_left_outlined),
          style: ElevatedButton.styleFrom(
              elevation: 0, primary: Colors.white, onPrimary: Colors.black54),
        ),
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Text(
          'Lịch sử đơn hàng',
          style: TextStyle(
              fontWeight: FontWeight.bold, fontSize: 20, color: Colors.black),
        ),
      ),
      body: Center(
        child: Text(
          'Bạn chưa có đơn hàng nào cả.',
          style: TextStyle(fontSize: 20, color: Colors.grey.shade800),
        ),
      ),
    );
  }
}
