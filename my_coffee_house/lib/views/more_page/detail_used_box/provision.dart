import 'package:flutter/material.dart';

class Provision extends StatefulWidget {
  static const route = '/provision';
  @override
  _ProvisionState createState() => _ProvisionState();
}

class _ProvisionState extends State<Provision> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        automaticallyImplyLeading: false,
        leadingWidth: 50,
        leading: ElevatedButton(
          onPressed: () => Navigator.of(context).pop(),
          child: Icon(Icons.keyboard_arrow_left_outlined),
          style: ElevatedButton.styleFrom(
              elevation: 0, primary: Colors.white, onPrimary: Colors.black54),
        ),
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Text(
          'Điều khoản',
          style: TextStyle(
              fontWeight: FontWeight.bold, fontSize: 20, color: Colors.black),
        ),
      ),
      body: Center(
        child: Text(
          'CHÍNH SÁCH BẢO MẬT THÔNG TIN',
          style: TextStyle(fontSize: 20, color: Colors.orange.shade800),
        ),
      ),
    );
  }
}
