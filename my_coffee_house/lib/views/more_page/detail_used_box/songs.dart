import 'package:flutter/material.dart';
import 'package:audio_video_progress_bar/audio_video_progress_bar.dart';

class Song extends StatefulWidget {
  static const route = '/song';
  @override
  _SongState createState() => _SongState();
}

class _SongState extends State<Song> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        automaticallyImplyLeading: false,
        leadingWidth: 50,
        leading: ElevatedButton(
          onPressed: () => Navigator.of(context).pop(),
          child: Icon(Icons.keyboard_arrow_left_outlined),
          style: ElevatedButton.styleFrom(
              elevation: 0, primary: Colors.white, onPrimary: Colors.black54),
        ),
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Text(
          'Nhạc đang phát',
          style: TextStyle(
              fontWeight: FontWeight.bold, fontSize: 20, color: Colors.black),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: Image(image: AssetImage('assets/moreImg/song1.png')),
                  )),
              Center(
                child: Text(
                  'Remaja',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      color: Colors.black),
                ),
              ),
              Center(
                child: Text(
                  'Hivi',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 15,
                      color: Colors.grey.shade600),
                ),
              ),
              ProgressBar(
                  progressBarColor: Colors.orange.shade800,
                  baseBarColor: Colors.black.withOpacity(0.24),
                  bufferedBarColor: Colors.black.withOpacity(0.24),
                  thumbColor: Colors.orange.shade800,
                  barHeight: 2.0,
                  thumbRadius: 5.0,
                  progress: Duration(milliseconds: 8000),
                  total: Duration(seconds: 350)),
              SizedBox(height: 20),
              Text(
                'Next song',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                    color: Colors.black),
              ),
              Container(
                padding: EdgeInsets.all(20),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Image(
                      image: AssetImage('assets/moreImg/song2.png'),
                      width: 50,
                      height: 50,
                    ),
                    VerticalDivider(
                      width: 20,
                    ),
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Homage',
                            style: TextStyle(
                              fontSize: 18,
                            ),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                            softWrap: false,
                          ),
                          Text(
                            'Mild High Club',
                            style: TextStyle(fontSize: 15),
                          ),
                          Text(
                            '4:48',
                            style: TextStyle(
                                fontSize: 15, color: Colors.grey.shade600),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
