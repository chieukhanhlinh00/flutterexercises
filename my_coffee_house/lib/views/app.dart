import 'package:flutter/material.dart';
import 'package:my_coffee_house/views/home_page/reward.dart';
import 'package:my_coffee_house/views/more_page/detail_used_box/history.dart';
import 'package:my_coffee_house/views/more_page/detail_used_box/provision.dart';
import 'package:my_coffee_house/views/more_page/detail_used_box/songs.dart';
import 'package:my_coffee_house/views/more_page/more_page.dart';
import 'package:my_coffee_house/views/order_page/order_page.dart';
import 'package:my_coffee_house/views/ourshop_page/ourshop_page.dart';
import 'package:my_coffee_house/views/search_page/search_drink.dart';
import 'package:my_coffee_house/views/search_page/search_shop.dart';
import 'package:my_coffee_house/views/voucher_page/tickets_voucher/ticket.dart';
import 'package:my_coffee_house/views/voucher_page/tickets_voucher/voucher.dart';
import 'package:my_coffee_house/views/voucher_page/voucher_page.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'home_page/grid_detail_homepage/detail_grid1.dart';
import 'home_page/grid_detail_homepage/detail_grid2.dart';
import 'home_page/grid_detail_homepage/detail_grid3.dart';
import 'home_page/home_page.dart';
import 'login_page/login_page.dart';
import 'order_page/payment/payment_page.dart';

class App extends StatelessWidget {
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "My Coffee House",
      theme: ThemeData(
        primaryColor: Colors.yellow[200],
        colorScheme: ColorScheme.fromSwatch()
            .copyWith(secondary: Colors.orangeAccent[400]),
      ),
      routes: {
        '/': (context) => MyHomePage(),
        '/home': (context) => HomePage(),
        '/more': (context) => MorePage(),
        '/order': (context) => OrderPage(),
        '/ourshop': (context) => OurshopPage(),
        '/voucher': (context) => VoucherPage(),
        '/searchdrink': (context) => SearchDrink(),
        '/searchshop': (context) => SearchShop(),
        '/login': (context) => Login(),
        '/reward': (context) => Reward(),
        '/payment': (context) => Payment(),
        '/history': (context) => History(),
        '/song': (context) => Song(),
        '/provision': (context) => Provision(),
        '/ticket': (context) => Ticket(),
        '/curvoucher': (context) => CurVoucher(),
        '/dtgrid1': (context) => DetailGrid1(),
        '/dtgrid2': (context) => DetailGrid2(),
        '/dtgrid3': (context) => DetailGrid3(),
      },
      initialRoute: '/',
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);
  static const route = '/';
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int currentIndex = 0;
  List screens = [
    HomePage(),
    OrderPage(),
    OurshopPage(),
    VoucherPage(),
    MorePage()
  ];

  void updateIndex(int value) {
    setState(() {
      currentIndex = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: screens[currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: currentIndex,
        onTap: updateIndex,
        selectedItemColor: Colors.yellow[900],
        selectedFontSize: 13,
        unselectedFontSize: 13,
        iconSize: 30,
        backgroundColor: Colors.white,
        items: [
          BottomNavigationBarItem(
            label: "Trang chủ",
            icon: FaIcon(
              FontAwesomeIcons.houseUser,
              size: 20,
            ),
          ),
          BottomNavigationBarItem(
            label: "Đặt hàng",
            icon: FaIcon(FontAwesomeIcons.mugHot, size: 20),
          ),
          BottomNavigationBarItem(
            label: "Cửa hàng",
            icon: FaIcon(FontAwesomeIcons.store, size: 20),
          ),
          BottomNavigationBarItem(
            label: "Tích điểm",
            icon: FaIcon(FontAwesomeIcons.ticketAlt, size: 20),
          ),
          BottomNavigationBarItem(
            label: "Khác",
            icon: FaIcon(FontAwesomeIcons.bars, size: 20),
          ),
        ],
      ),
    );
  }
}
