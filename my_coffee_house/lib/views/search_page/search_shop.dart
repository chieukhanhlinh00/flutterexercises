import 'package:flutter/material.dart';
import 'package:my_coffee_house/views/ourshop_page/shops.dart';

class SearchShop extends StatefulWidget{
  static const route = '/searchshop';
  @override
  _SearchShopState createState() => _SearchShopState();
}


class _SearchShopState extends State<SearchShop>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        leadingWidth: 50,
        leading: ElevatedButton(
          onPressed: () => Navigator.of(context).pop(),
          child: Icon(Icons.keyboard_arrow_left_outlined),
          style: ElevatedButton.styleFrom(
              elevation: 0, primary: Colors.white, onPrimary: Colors.black54),
        ),
        backgroundColor: Colors.white,
        title: Container(
          width: double.infinity,
          height: 40,
          decoration: BoxDecoration(
              color: Colors.black38.withAlpha(10), borderRadius: BorderRadius.circular(5)),
          child: TextField(
              decoration: InputDecoration(
                  prefixIcon: Icon(Icons.search),
                  hintText: 'Nhập tên đường, quận huyện...',
                  border: InputBorder.none),
            ),
        ),
      ),
      body: Shops(),
    );
  }

}
