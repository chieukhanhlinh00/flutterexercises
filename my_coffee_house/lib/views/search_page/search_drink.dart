import 'package:flutter/material.dart';

import '../order_page/drinks.dart';

class SearchDrink extends StatefulWidget{
  static const route = '/searchdrink';
  @override
  _SearchDrinkState createState() => _SearchDrinkState();
}


class _SearchDrinkState extends State<SearchDrink>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        leadingWidth: 50,
        leading: ElevatedButton(
          onPressed: () => Navigator.of(context).pop(),
          child: Icon(Icons.keyboard_arrow_left_outlined),
          style: ElevatedButton.styleFrom(
              elevation: 0, primary: Colors.white, onPrimary: Colors.black54),
        ),
        backgroundColor: Colors.white,
        title: Container(
          width: double.infinity,
          height: 40,
          decoration: BoxDecoration(
              color: Colors.black38.withAlpha(10), borderRadius: BorderRadius.circular(5)),
          child: TextField(
            decoration: InputDecoration(
                prefixIcon: Icon(Icons.search),
                hintText: 'Nhập tên đường, quận huyện...',
                border: InputBorder.none),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: ListDrinks(),
      )
    );
  }

}
