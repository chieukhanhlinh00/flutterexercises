import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:my_coffee_house/views/voucher_page/tickets_voucher/ticket.dart';
import 'package:my_coffee_house/views/voucher_page/voucher_screen.dart';

import '../login_page/login_page.dart';
import 'accumulate_screen.dart';

class VoucherPage extends StatefulWidget {
  static const route = '/voucher';
  State<VoucherPage> createState() => _VoucherPageState();
}

class _VoucherPageState extends State<VoucherPage> {
  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.white,
            title: Text(
              'Tích điểm',
              style:
                  TextStyle(color: Colors.black, fontWeight: FontWeight.w600),
            ),
            actions: [
              Container(
                width: 40,
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey, width: 0.5),
                    color: Colors.white,
                    shape: BoxShape.circle,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.7),
                        spreadRadius: 1,
                        blurRadius: 3,
                      )
                    ]),
                child: IconButton(
                  color: Colors.black,
                  iconSize: 20,
                  icon: FaIcon(FontAwesomeIcons.ticketAlt),
                  onPressed: () => Navigator.of(context)
                      .push(MaterialPageRoute(builder: (_) => Ticket())),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 8, right: 8),
                width: 40,
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey, width: 0.5),
                    color: Colors.white,
                    shape: BoxShape.circle,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.7),
                        spreadRadius: 1,
                        blurRadius: 3,
                      )
                    ]),
                child: IconButton(
                  color: Colors.black,
                  iconSize: 20,
                  icon: Icon(Icons.notifications),
                  onPressed: () {
                    Navigator.of(context).pushNamed(Login.route);
                  },
                ),
              ),
            ],
            bottom: PreferredSize(
              preferredSize: Size.fromHeight(kToolbarHeight),
              child: Padding(
                padding: EdgeInsets.all(10.0),
                child: Align(
                  child: TabBar(
                    labelPadding: EdgeInsets.symmetric(horizontal: 16.0),
                    isScrollable: true,
                    labelColor: Colors.yellow[900],
                    unselectedLabelColor: Colors.black,
                    indicator: BoxDecoration(
                      borderRadius: BorderRadius.circular(40),
                      border:
                          Border.all(color: Colors.yellow.shade900, width: 1),
                    ),
                    tabs: [
                      Tab(
                        child: Text(
                          "Tích điểm",
                        ),
                      ),
                      Tab(
                        child: Text('Đổi ưu đãi'),
                      ),
                    ],
                  ),
                  alignment: Alignment.centerLeft,
                ),
              ),
            ),
          ),
          body: Container(
            color: Colors.grey[100],
            child: TabBarView(
              physics: NeverScrollableScrollPhysics(),
              children: [
                AccumulateScreen(),
                VoucherScreen(),
              ],
            ),
          )),
    );
  }
}
