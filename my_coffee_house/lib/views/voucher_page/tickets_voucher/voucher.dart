import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../../order_page/list_drinks/hi_tea.dart';

class CurVoucher extends StatefulWidget {
  static const route = '/curvoucher';
  @override
  _CurVoucherState createState() => _CurVoucherState();
}

class _CurVoucherState extends State<CurVoucher> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Container(
              color: Colors.grey[100],
              child: Column(
                children: [
                  Container(
                    height: 180,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(30),
                            topRight: Radius.circular(30)),
                        image: DecorationImage(
                            image: AssetImage('assets/homeImg/banner7.png'),
                            fit: BoxFit.cover)),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Text(
                      'Mừng ngày của mẹ, Nhà tặng bạn deal 30%',
                      style:
                          TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                      'Ưu đãi 30% các món từ Hi Tea, áp dụng cho mua tại cửa hàng hoặc đặt ship',
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                      'Cùng Hi Tea mang đến các món ngon cho người phụ nữ thân yêu nhất nha!',
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: HiTea(),
                  ),
                ],
              ),
            ),
          ),
          Align(
            child: IconButton(
                onPressed: () => Navigator.pop(context),
                icon: FaIcon(
                  FontAwesomeIcons.timesCircle,
                  size: 30,
                  color: Colors.grey.shade600,
                )),
            alignment: Alignment.topLeft,
          ),
        ],
      ),
    );
  }
}
