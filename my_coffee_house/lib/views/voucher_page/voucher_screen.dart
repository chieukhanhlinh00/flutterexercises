import 'package:flutter/material.dart';

import '../login_page/login_page.dart';

class VoucherScreen extends StatefulWidget{
  const VoucherScreen({Key? key}) : super(key: key);

  static const route = '/voucher/vouchers';
  @override
  _VoucherScreenState createState() => _VoucherScreenState();
}

class _VoucherScreenState extends State<VoucherScreen>{
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
      padding: EdgeInsets.all(20),
      child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Image(image: AssetImage('assets/cardImg/bg.png')),
          SizedBox(
            height: 50,
          ),
          Text('Hãy đăng nhập để xem các ưu đãi dành riêng cho thành viên của Nhà nhé!',
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700, color: Colors.grey),
            textAlign: TextAlign.center,
          ),
          SizedBox(
            height: 30,
          ),
          TextButton(onPressed: ()=> {Navigator.of(context).pushNamed(Login.route)},
              child: Text('Đăng nhập', style: TextStyle(fontSize: 22),),
            style: TextButton.styleFrom(
              primary: Colors.white,
              backgroundColor: Colors.yellow[900],
              side: BorderSide(color: Colors.deepOrange, width: 1),
              minimumSize: Size(450, 60),
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            ),
          ),
        ],
      ),
      ),
    );
  }
}

