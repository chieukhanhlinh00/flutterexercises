import 'package:flutter/material.dart';

import '../../../models/data_voucher.dart';

class Card1 extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
      padding: EdgeInsets.only(bottom: 20),
      child: ListView.builder(
          shrinkWrap: true,
          itemCount: titleList1.length,
          scrollDirection: Axis.vertical,
          itemBuilder: (context, index) => Container(
            decoration: BoxDecoration(
              border: Border.symmetric(horizontal: BorderSide(
                color: Colors.white, width:  0.2),
              ),
              ),
            child: Card(
              margin: EdgeInsets.only(bottom: 1),
              elevation: 0.0,
              child: Padding(
                padding: EdgeInsets.only(left: 20),
              child: ListTile(
                title: Text(titleList1[index], style: TextStyle(fontSize: 18),),
                leading: GestureDetector(
                  child: CircleAvatar(
                    backgroundImage: AssetImage(imgList1[index]),
                    maxRadius: 25,
                    backgroundColor: Colors.grey[100],
                  ),
                ),
              ),
            ),
            ),
            ),
          ),
      ),
    );
  }
}

class Card2 extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 20),
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: titleList2.length,
        scrollDirection: Axis.vertical,
        itemBuilder: (context, index) => Container(
          decoration: BoxDecoration(
            border: Border.symmetric(horizontal: BorderSide(
                color: Colors.white, width:  0.2),
            ),
          ),
          child: Card(
            margin: EdgeInsets.only(bottom: 1),
            elevation: 0.0,
            child: Padding(
              padding: EdgeInsets.only(left: 20),
              child: ListTile(
                title: Text(titleList2[index], style: TextStyle(fontSize: 18),),
                leading: GestureDetector(
                  child: CircleAvatar(
                    backgroundImage: AssetImage(imgList2[index]),
                    maxRadius: 25,
                    backgroundColor: Colors.grey[100],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class Card3 extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 20),
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: titleList3.length,
        scrollDirection: Axis.vertical,
        itemBuilder: (context, index) => Container(
          decoration: BoxDecoration(
            border: Border.symmetric(horizontal: BorderSide(
                color: Colors.white, width:  0.2),
            ),
          ),
          child: Card(
            margin: EdgeInsets.only(bottom: 1),
            elevation: 0.0,
            child: Padding(
              padding: EdgeInsets.only(left: 20),
              child: ListTile(
                title: Text(titleList3[index], style: TextStyle(fontSize: 18),),
                leading: GestureDetector(
                  child: CircleAvatar(
                    backgroundImage: AssetImage(imgList3[index]),
                    maxRadius: 25,
                    backgroundColor: Colors.grey[100],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class Card4 extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 20),
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: titleList4.length,
        scrollDirection: Axis.vertical,
        itemBuilder: (context, index) => Container(
          decoration: BoxDecoration(
            border: Border.symmetric(horizontal: BorderSide(
                color: Colors.white, width:  0.2),
            ),
          ),
          child: Card(
            margin: EdgeInsets.only(bottom: 1),
            elevation: 0.0,
            child: Padding(
              padding: EdgeInsets.only(left: 20),
              child: ListTile(
                title: Text(titleList4[index], style: TextStyle(fontSize: 18),),
                leading: GestureDetector(
                  child: CircleAvatar(
                    backgroundImage: AssetImage(imgList4[index]),
                    maxRadius: 25,
                    backgroundColor: Colors.grey[100],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class Card5 extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 20),
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: titleList5.length,
        scrollDirection: Axis.vertical,
        itemBuilder: (context, index) => Container(
          decoration: BoxDecoration(
            border: Border.symmetric(horizontal: BorderSide(
                color: Colors.white, width:  0.2),
            ),
          ),
          child: Card(
            margin: EdgeInsets.only(bottom: 1),
            elevation: 0.0,
            child: Padding(
              padding: EdgeInsets.only(left: 20),
              child: ListTile(
                title: Text(titleList5[index], style: TextStyle(fontSize: 18),),
                leading: GestureDetector(
                  child: CircleAvatar(
                    backgroundImage: AssetImage(imgList5[index]),
                    maxRadius: 25,
                    backgroundColor: Colors.grey[100],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}