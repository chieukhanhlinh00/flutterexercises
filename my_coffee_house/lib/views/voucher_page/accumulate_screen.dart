import 'package:flutter/material.dart';

import '../login_page/login_page.dart';
import 'cards/card_options.dart';

class AccumulateScreen extends StatefulWidget{
  static const route = '/voucher/accumulate';
  @override
  _AccumulateScreenState createState() => _AccumulateScreenState();
}

class _AccumulateScreenState extends State<AccumulateScreen>{
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Column(
            children: [
              Container(
                  child: topBox()
              ),
              Container(
                child: bottomBox() ,
              )
            ]
        )
    );
  }
}

class topBox extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Align(
        child: Container(
          child: Center(
            child:Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 45, right: 45, top: 60, bottom: 20),
                    child: Text('Sử dụng app để tích điểm và đổi '
                        'những ưu đãi chỉ dành riêng cho thanh viên bạn nhé !',
                      style: TextStyle(color: Colors.white, fontSize: 18),
                    ),
                  ),
                  TextButton(onPressed: ()=> {Navigator.of(context).pushNamed(Login.route)},
                    child: Text('ĐĂNG NHẬP', style: TextStyle(fontSize: 18),),
                    style: TextButton.styleFrom(
                      primary: Colors.white,
                      backgroundColor: Colors.orange.withOpacity(0.5),
                      side: BorderSide(color: Colors.deepOrange, width: 1),
                      minimumSize: Size(250, 40),
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
                    ),
                  ),
                ]
            ),
          ),
          height: 230,
          margin: EdgeInsets.all(20.0),
          decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [
                  Colors.yellow.shade900,
                  Colors.orange.shade900,
                ],
              ),
              borderRadius: BorderRadius.circular(18),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.7),
                  spreadRadius: 3,
                  blurRadius: 8,
                )
              ]
          ),
        ),
        alignment: Alignment.topCenter,
    );
  }
}
class Choice{
  final String title;
  final String image;

  Choice(this.image, this.title);
}
class bottomBox extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    List<Choice> tabs = [
      Choice('assets/cardImg/card1.png', 'Mới'),
      Choice('assets/cardImg/card2.png', 'Đồng'),
      Choice('assets/cardImg/card3.png', 'Bạc'),
      Choice('assets/cardImg/card4.png', 'Vàng'),
      Choice('assets/cardImg/card5.png', 'Kim'),
    ];

    return DefaultTabController(
    length: 5,
      child: Column(
        children: [
          Container(
            color: Colors.white,
            padding: EdgeInsets.only(top: 20, bottom: 20),
            child: Center(
              child: Column(
                children: [
                  Text('Thăng hạng dễ dàng',
                    style: TextStyle(fontSize: 23, fontWeight: FontWeight.w600 ),),

                  Text('Quyền lợi đa dạng & hấp dẫn',
                    style: TextStyle(fontSize: 23, fontWeight: FontWeight.w600 ),),

                  Container(
                    height: 100,
                    margin: EdgeInsets.all(10),
                    child: TabBar(
                      unselectedLabelColor: Colors.black,
                      indicatorWeight: 1,
                      indicator: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(color: Colors.grey.shade300, width: 1),
                          boxShadow: [
                            BoxShadow( color: Colors.grey.withOpacity(0.7),
                              offset: const Offset(
                                0.0,
                                5.0,
                              ),
                              spreadRadius: 1,
                              blurRadius: 3,
                            ), ]
                      ),
                      tabs: tabs
                          .map((Choice tab) =>
                          Tab(icon: Image(image: AssetImage(tab.image),
                            width: 60, ),
                            text: tab.title,
                          )
                      ).toList(),
                    ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: TabBarView(
              physics: NeverScrollableScrollPhysics(),
              children: [
                Card1(),
                Card2(),
                Card3(),
                Card4(),
                Card5(),
              ],
            ),
          ),
        ],
      ),
    );
  }

}
