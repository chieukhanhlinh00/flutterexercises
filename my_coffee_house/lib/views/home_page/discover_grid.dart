import 'package:flutter/material.dart';

import 'grid_homepage/grid_screen.dart';

class GridDisc extends StatefulWidget{
  @override
  State<GridDisc> createState() => _GridDiscState();

}

class _GridDiscState extends State<GridDisc>{
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Padding(
      padding: EdgeInsets.only(left: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Text('Khám phá thêm', style: TextStyle(
                  color: Colors.black,
                  fontSize: 25,
                  fontWeight: FontWeight.bold
              ),),
              SizedBox(width: 10,),
              Image(image: AssetImage('assets/homeImg/icon2.png'),),
            ],
          ),

          PreferredSize(
            preferredSize: Size.fromHeight(kToolbarHeight),
            child: Padding(
              padding: EdgeInsets.all(0.0),
              child: Align(
                child: TabBar(
                  labelPadding: EdgeInsets.symmetric(horizontal: 16.0),
                  isScrollable: true,
                  labelColor: Colors.yellow[900],
                  unselectedLabelColor: Colors.black,
                  indicator: BoxDecoration(
                    borderRadius: BorderRadius.circular(40),
                    border: Border.all(color: Colors.yellow.shade900, width: 1),
                  ),
                  tabs: [
                    Tab(
                      child: Text("Ưu đãi đặc biệt", style: TextStyle(fontSize: 18),),
                    ),
                    Tab(child: Text('Cập nhật từ Nhà',style: TextStyle(fontSize: 18)), ),
                    Tab(child: Text('#CoffeeLover',style: TextStyle(fontSize: 18)), ),
                  ],
                ),
                alignment: Alignment.centerLeft,
              ),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height*1.5,
            child: TabBarView(
              // physics: NeverScrollableScrollPhysics(),
              children: [
                GridHome1(),
                GridHome2(),
                GridHome3(),
              ],
            ),
          ),
        ],
      ),
      ),

    );
  }

}