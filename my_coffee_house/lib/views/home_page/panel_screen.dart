import 'package:flutter/material.dart';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:my_coffee_house/views/voucher_page/tickets_voucher/voucher.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import 'discover_grid.dart';

class PanelScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          imageButton(),
          SizedBox(
            height: 10,
          ),
          bannerAds(),
          SizedBox(
            height: 20,
          ),
          bannerDiscount(),
          SizedBox(
            height: 30,
          ),
          GridDisc(),
        ],
      ),
    );
  }
}

class imageButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => {},
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        height: 100,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            border: Border.all(color: Colors.grey.shade300, width: 1),
            borderRadius: BorderRadius.circular(15),
            image: DecorationImage(
              image: AssetImage('assets/homeImg/delivery.png'),
              fit: BoxFit.fitWidth,
            )),
      ),
    );
  }
}

class bannerAds extends StatefulWidget {
  const bannerAds({Key? key}) : super(key: key);
  State<bannerAds> createState() => _bannerAdsState();
}

class _bannerAdsState extends State<bannerAds> {
  int activeIndex = 0;
  final imgBanner = [
    'assets/homeImg/banner1.png',
    'assets/homeImg/banner2.png',
    'assets/homeImg/banner3.png',
    'assets/homeImg/banner4.png',
    'assets/homeImg/banner5.png',
    'assets/homeImg/banner6.png',
  ];
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CarouselSlider.builder(
            itemCount: imgBanner.length,
            options: CarouselOptions(
              height: 180,
              enlargeCenterPage: false,
              autoPlay: true,
              aspectRatio: 16 / 7,
              autoPlayCurve: Curves.fastOutSlowIn,
              enableInfiniteScroll: false,
              autoPlayAnimationDuration: Duration(milliseconds: 800),
              viewportFraction: 1,
              onPageChanged: (index, reaspn) =>
                  setState(() => activeIndex = index),
            ),
            itemBuilder: (BuildContext context, int index, int realIndex) {
              final _imgBanner = imgBanner[index];

              return buildImage(_imgBanner, index);
            },
          ),
          SizedBox(
            height: 10,
          ),
          buildIndicator(),
        ],
      ),
    );
  }

  Widget buildImage(String imgBanner, int index) => Container(
          child: Container(
        width: 400.0,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          //set border radius to 50% of square height and width
          image: DecorationImage(
            image: AssetImage(imgBanner),
            fit: BoxFit.cover, //change image fill type
          ),
        ),
      ));

  Widget buildIndicator() => AnimatedSmoothIndicator(
        activeIndex: activeIndex,
        count: imgBanner.length,
        effect: SlideEffect(
            spacing: 5.0,
            radius: 4.0,
            dotWidth: 20.0,
            dotHeight: 3.0,
            dotColor: Colors.grey.shade300,
            activeDotColor: Colors.grey),
      );
}

class bannerDiscount extends StatefulWidget {
  const bannerDiscount({Key? key}) : super(key: key);
  State<bannerDiscount> createState() => _bannerDiscountState();
}

class _bannerDiscountState extends State<bannerDiscount> {
  final imgDisc = [
    'assets/homeImg/banner7.png',
    'assets/homeImg/banner8.png',
  ];
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(left: 20),
          child: Row(
            children: [
              Image(
                image: AssetImage('assets/homeImg/icon1.png'),
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                'Bộ sưu tập',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 25,
                    fontWeight: FontWeight.bold),
              )
            ],
          ),
        ),
        SizedBox(
          height: 10,
        ),
        GestureDetector(
          onTap: () => {
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (_) => CurVoucher())),
          },
          child: CarouselSlider.builder(
            itemCount: imgDisc.length,
            options: CarouselOptions(
              height: 190,
              enlargeCenterPage: false,
              autoPlay: true,
              aspectRatio: 16 / 7,
              autoPlayCurve: Curves.fastOutSlowIn,
              enableInfiniteScroll: false,
              autoPlayAnimationDuration: Duration(milliseconds: 800),
              viewportFraction: 0.9,
            ),
            itemBuilder: (BuildContext context, int index, int realIndex) {
              final _imgDisc = imgDisc[index];

              return buildImageDisc(_imgDisc, index);
            },
          ),
        ),
      ],
    );
  }

  Widget buildImageDisc(String imgDisc, int index) => Container(
      margin: EdgeInsets.symmetric(horizontal: 0),
      child: Container(
        margin: EdgeInsets.all(5),
        height: 100.0,
        width: 400.0,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          //set border radius to 50% of square height and width
          image: DecorationImage(
            image: AssetImage(imgDisc),
            fit: BoxFit.cover, //change image fill type
          ),
        ),
      ));
}
