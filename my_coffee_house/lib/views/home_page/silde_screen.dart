import 'package:flutter/material.dart';
import 'package:my_coffee_house/views/home_page/reward.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

import '../login_page/login_page.dart';
import 'panel_screen.dart';

class SlideScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    BorderRadiusGeometry radius = BorderRadius.only(
      topLeft: Radius.circular(17.0),
      topRight: Radius.circular(17.0),
    );
    return Stack(
      children: [
        loginBoxPanel(),
        SlidingUpPanel(
          minHeight: MediaQuery.of(context).size.height * 0.315,
          maxHeight: MediaQuery.of(context).size.height,
          borderRadius: radius,
          backdropEnabled: true,
          panel: SingleChildScrollView(child: PanelScreen()),
          collapsed: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: radius,
            ),
            child: Padding(
              padding: EdgeInsets.only(left: 5, right: 5, top: 13),
              child: Container(
                height: 10,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      height: 5,
                      width: 60,
                      margin: EdgeInsets.only(bottom: 7),
                      decoration: BoxDecoration(
                          color: Colors.grey,
                          borderRadius: BorderRadius.circular(20)),
                    ),
                    GestureDetector(
                      onTap: () => {},
                      child: Container(
                        margin: EdgeInsets.only(left: 16.5, right: 16.5),
                        height: 120,
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: Colors.grey.shade300, width: 1),
                            borderRadius: BorderRadius.circular(15),
                            image: DecorationImage(
                              image: AssetImage('assets/homeImg/delivery.png'),
                              fit: BoxFit.fitWidth,
                            )),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class loginBoxPanel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.amber.shade50,
      child: Container(
        // height: MediaQuery.of(context).size.height * 0.4,
        width: MediaQuery.of(context).size.width,
        margin: EdgeInsets.only(left: 20.0, right: 20, top: 20, bottom: 40),
        decoration: BoxDecoration(
            color: Colors.white,
            image: DecorationImage(
                fit: BoxFit.fitWidth,
                image: AssetImage('assets/homeImg/bg_box.jpg'),
                colorFilter: ColorFilter.mode(
                    Colors.white.withOpacity(0.5), BlendMode.dstATop)),
            borderRadius: BorderRadius.circular(18),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.7),
                spreadRadius: 3,
                blurRadius: 8,
              )
            ]),
        child: Center(
          child: Column(children: [
            Padding(
              padding:
                  EdgeInsets.only(left: 25, right: 25, top: 25, bottom: 20),
              child: Column(children: [
                Text(
                  'Đăng nhập',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 25,
                      fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'Sử dụng app để tích điểm và đổi những ưu đãi chỉ dành riêng cho'
                  ' thành viên bạn nhé !',
                  style: TextStyle(color: Colors.black, fontSize: 20),
                ),
              ]),
            ),
            TextButton(
              onPressed: () => {Navigator.of(context).pushNamed(Login.route)},
              child: Text(
                'Đăng nhập',
                style: TextStyle(fontSize: 18),
              ),
              style: TextButton.styleFrom(
                primary: Colors.white,
                backgroundColor: Colors.yellow[900],
                side: BorderSide(
                    color: Colors.grey.shade400.withOpacity(0.5), width: 1),
                minimumSize: Size(220, 45),
                elevation: 15,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
              ),
            ),
            // SizedBox(height: 20,),

            Container(
              margin: EdgeInsets.all(20),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                border: Border.all(color: Colors.grey.shade300, width: 1),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.7),
                    offset: const Offset(
                      0.0,
                      2.0,
                    ),
                    spreadRadius: 0.1,
                    blurRadius: 0.1,
                  ),
                ],
              ),
              child: Padding(
                padding: EdgeInsets.only(left: 20),
                child: ListTile(
                  onTap: () => Navigator.of(context)
                      .push(MaterialPageRoute(builder: (_) => Reward())),
                  title: Text(
                    "My Coffee House's Reward",
                    style: TextStyle(fontSize: 18),
                  ),
                  trailing: Icon(
                    Icons.keyboard_arrow_right_rounded,
                    color: Colors.black54,
                  ),
                ),
              ),
            ),
          ]),
        ),
      ),
    );
  }
}
