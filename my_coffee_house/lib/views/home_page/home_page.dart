import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:my_coffee_house/views/voucher_page/tickets_voucher/ticket.dart';

import '../login_page/login_page.dart';
import 'silde_screen.dart';

class HomePage extends StatefulWidget {
  static const route = '/home';
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: Container(
          child: Row(children: [
            Image(image: AssetImage('assets/homeImg/title1.png')),
            SizedBox(
              width: 10,
            ),
            Text(
              'Chào bạn mới',
              style:
                  TextStyle(color: Colors.black, fontWeight: FontWeight.w600),
            ),
            Image(image: AssetImage('assets/homeImg/title2.png')),
          ]),
        ),
        actions: [
          Container(
            width: 40,
            decoration: BoxDecoration(
                border: Border.all(color: Colors.grey, width: 0.5),
                color: Colors.white,
                shape: BoxShape.circle,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.7),
                    spreadRadius: 1,
                    blurRadius: 3,
                  )
                ]),
            child: IconButton(
              color: Colors.black,
              iconSize: 20,
              icon: FaIcon(FontAwesomeIcons.ticketAlt),
              onPressed: () => Navigator.of(context)
                  .push(MaterialPageRoute(builder: (_) => Ticket())),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 8, right: 8),
            width: 40,
            decoration: BoxDecoration(
                border: Border.all(color: Colors.grey, width: 0.5),
                color: Colors.white,
                shape: BoxShape.circle,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.7),
                    spreadRadius: 1,
                    blurRadius: 3,
                  )
                ]),
            child: IconButton(
              color: Colors.black,
              iconSize: 20,
              icon: Icon(Icons.notifications),
              onPressed: () {
                Navigator.of(context).pushNamed(Login.route);
              },
            ),
          ),
        ],
      ),
      body: SlideScreen(),
    );
  }
}
