import 'package:flutter/material.dart';

class DetailGrid3 extends StatefulWidget {
  static const route = '/dtgrid3';
  @override
  _DetailGrid3State createState() => _DetailGrid3State();
}

class _DetailGrid3State extends State<DetailGrid3> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 1,
          automaticallyImplyLeading: false,
          leadingWidth: 50,
          leading: ElevatedButton(
            onPressed: () => Navigator.of(context).pop(),
            child: Icon(Icons.keyboard_arrow_left_outlined),
            style: ElevatedButton.styleFrom(
                elevation: 0, primary: Colors.white, onPrimary: Colors.black54),
          ),
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Text(
            '#CoffeeLover',
            style: TextStyle(
                fontWeight: FontWeight.bold, fontSize: 20, color: Colors.black),
          ),
        ),
        body: SingleChildScrollView(
          child: Container(
            color: Colors.white,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Text(
                    'Nghệ thuật pha chế -V60'.toUpperCase(),
                    style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                  ),
                ),
                Image(
                  image: AssetImage('assets/homeImg/dtgrid3.png'),
                  width: MediaQuery.of(context).size.width,
                ),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Text(
                    'Đúng với tên gọi, V60 mang hình dáng một Vector với góc 60 độ. Vector này được tạo nên từ parabol "chuẩn" y = x² xuất hiện rất nhiều trong cuộc sống hàng ngày.Vì vậy bình V60 được mệnh danh là Thiết kế của Tự nhiên - The shape of Nature',
                    style: TextStyle(fontSize: 15),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Image(
                    image: AssetImage('assets/homeImg/dtgrid3.png'),
                    width: MediaQuery.of(context).size.width,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Text(
                    'Step 1 - Đặt giấy lọc vào V60'
                    ' Sau đó tráng qua nước sôi để khử đi mùi của giấy lọc, đồng thời làm nóng dripper và server',
                    style: TextStyle(
                      fontSize: 15,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Text(
                    'Step 2 - Cân và xay hạt cà phê'
                    'V60 thích hợp với cách rang sáng màu, mang lại nhiều hương vị trái cây, hoa. Những hạt cà phê nổi tiếng trên thế giới như ở Kenya, Ethiopia,… sẽ có hương vị tốt nhất khi pha với cách pha này.',
                    style: TextStyle(
                      fontSize: 15,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
