import 'package:flutter/material.dart';

class DetailGrid1 extends StatefulWidget {
  static const route = '/dtgrid1';
  @override
  _DetailGrid1State createState() => _DetailGrid1State();
}

class _DetailGrid1State extends State<DetailGrid1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 1,
          automaticallyImplyLeading: false,
          leadingWidth: 50,
          leading: ElevatedButton(
            onPressed: () => Navigator.of(context).pop(),
            child: Icon(Icons.keyboard_arrow_left_outlined),
            style: ElevatedButton.styleFrom(
                elevation: 0, primary: Colors.white, onPrimary: Colors.black54),
          ),
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Text(
            'Ưu đãi đặc biệt',
            style: TextStyle(
                fontWeight: FontWeight.bold, fontSize: 20, color: Colors.black),
          ),
        ),
        body: SingleChildScrollView(
          child: Container(
            color: Colors.white,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Text(
                    'nước thiệt chill - deal tiết kiệm, nhà & shopeepay mời bạn ngay 50%'
                        .toUpperCase(),
                    style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                  ),
                ),
                Image(
                  image: AssetImage('assets/homeImg/dtgrid1.png'),
                  width: MediaQuery.of(context).size.width,
                ),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Text(
                    'Deadline có thể quên nhưng deal ngon thì nhớ lưu lại ngay bạn nhé. Mời bạn ngay 50% để thưởng thức nước ngon siêu chill mùa hè này.',
                    style: TextStyle(fontSize: 15),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Image(
                    image: AssetImage('assets/homeImg/dtgrid1.png'),
                    width: MediaQuery.of(context).size.width,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Text(
                    'Dành cho bạn mới'.toUpperCase(),
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Text(
                    'Giảm tối đa 50k, áp dụng tối đa 1 lần/khách hàng, từ ngày 1/5 - 30/5/2022',
                    style: TextStyle(
                      fontSize: 15,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Text(
                    'Dành cho bạn thân'.toUpperCase(),
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Text(
                    'Giảm trực tiếp 50% tối đa 20k khi nhập mã SPPCHCPEU. Dùng tối đa 2 lần/khách hàng/ tháng. Từ ngày 1/5 - 30/5/2022',
                    style: TextStyle(
                      fontSize: 15,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
