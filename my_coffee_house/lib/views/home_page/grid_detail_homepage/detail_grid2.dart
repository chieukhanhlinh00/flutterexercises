import 'package:flutter/material.dart';

class DetailGrid2 extends StatefulWidget {
  static const route = '/dtgrid2';
  @override
  _DetailGrid2State createState() => _DetailGrid2State();
}

class _DetailGrid2State extends State<DetailGrid2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 1,
          automaticallyImplyLeading: false,
          leadingWidth: 50,
          leading: ElevatedButton(
            onPressed: () => Navigator.of(context).pop(),
            child: Icon(Icons.keyboard_arrow_left_outlined),
            style: ElevatedButton.styleFrom(
                elevation: 0, primary: Colors.white, onPrimary: Colors.black54),
          ),
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Text(
            'Cập nhật từ Nhà',
            style: TextStyle(
                fontWeight: FontWeight.bold, fontSize: 20, color: Colors.black),
          ),
        ),
        body: SingleChildScrollView(
          child: Container(
            color: Colors.white,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Text(
                    'Có công tích lũy bean - có ngày nhận deal đậm'
                        .toUpperCase(),
                    style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                  ),
                ),
                Image(
                  image: AssetImage('assets/homeImg/dtgrid2.png'),
                  width: MediaQuery.of(context).size.width,
                ),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Text(
                    'Sau bao ngày tích góp bean miệt mài, bạn còn chưa đổi bean đi nè. Bean tích lũy từ 1/7/2021 - 30/9/2021 sẽ hết hạn vào ngày 1/6/2022 này đó.',
                    style: TextStyle(fontSize: 15),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Image(
                    image: AssetImage('assets/homeImg/dtgrid2.png'),
                    width: MediaQuery.of(context).size.width,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Text(
                    'Thiệt nhiều deal "đậm" dành cho bạn đây',
                    style: TextStyle(
                      fontSize: 15,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Text(
                    'Mua 1 tặng 1 cà phê size M. Mua 1 tặng 1 trà sữa size M. Cash voucher 10.000đ. Voucher miễn phí up size nước'
                    'Miễn phí 1 cà phê Việt Nam size M'
                    'Miễn phí 1 trà sữa size M'
                    'Miễn phí 1 trà trái cây size M'
                    'Miễn phí 1 trà sữa Oolong nướng trân châu size M'
                    'Miễn phí 1 hồng trà sữa chân châu size M',
                    style: TextStyle(
                      fontSize: 15,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
