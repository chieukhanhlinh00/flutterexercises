import 'package:flutter/material.dart';

class Reward extends StatefulWidget{
  static const route = '/reward';
  @override
  _RewardState createState() => _RewardState();
}


class _RewardState extends State<Reward>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        leadingWidth: 50,
        leading: ElevatedButton(
          onPressed: () => Navigator.of(context).pop(),
          child: Icon(Icons.keyboard_arrow_left_outlined),
          style: ElevatedButton.styleFrom(
              elevation: 0, primary: Colors.white, onPrimary: Colors.black54),
        ),
        backgroundColor: Colors.white,
        centerTitle: true,
        title:  Text("My Coffee House's Reward",
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20, color: Colors.black),)
        ),
      body: Stack(
          children: [

            SingleChildScrollView(
              child:  Container(
                color: Colors.white,
                margin: EdgeInsets.only(top: 20, left: 20, right: 20, bottom: 100),
                padding: EdgeInsets.all(20),
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("My Coffee House's Reward".toUpperCase(),
                        style: TextStyle(color: Colors.grey.shade800, fontSize: 15, fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),

                      Text("với my coffee house's reward".toUpperCase(),
                        style: TextStyle(color: Colors.black, fontSize: 30, fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),

                      Image(image: AssetImage('assets/homeImg/reImg1.png')),

                      Text("Tích BEAN dễ dàng - Thăng hạng nhanh hơn",
                        style: TextStyle(color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                      Text("Mô hình thăng hạng thành viên mới dễ dàng hơn",
                        style: TextStyle(color: Colors.black, fontSize: 20),
                        textAlign: TextAlign.center,
                      ),

                      Image(image: AssetImage('assets/homeImg/reImg2.png')),

                      Text("Cửa hàng ưu đãi",
                        style: TextStyle(color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),

                      Text("Thoải mái đổi BEAN để nhận nhiều phần quà cực hấp dẫn!",
                        style: TextStyle(color: Colors.black, fontSize: 20),
                        textAlign: TextAlign.center,
                      ),

                      Image(image: AssetImage('assets/homeImg/reImg3.png')),

                      Text("Đặc quyền Kim Cương",
                        style: TextStyle(color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),

                      Text("Tận hưởng đặc quyền chỉ dành riêng cho thành viên Kim Cương",
                        style: TextStyle(color: Colors.black, fontSize: 20),
                        textAlign: TextAlign.center,
                      ),

                    ],
                  ),
                ),
              ),
            ),


            Positioned(
              bottom: 0,
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 100,
                padding: EdgeInsets.all(20),
                color: Colors.white,
                child: TextButton(onPressed: ()=>{},
                    child: Text('Đăng kí thành viên', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
                    style: TextButton.styleFrom(
                      primary: Colors.white,
                      backgroundColor: Colors.orange.shade800,
                      minimumSize: Size(400, 50),
                    ),
                ),
              ),
            ),
          ],
        ),

    );
  }

}
