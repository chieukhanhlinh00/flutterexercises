import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:my_coffee_house/views/search_page/search_drink.dart';

import 'drinks.dart';
import 'order_page_body.dart';

class OrderPage extends StatefulWidget {
  static const route = '/order';
  State<OrderPage> createState() => _OrderPageState();
}

class _OrderPageState extends State<OrderPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        centerTitle: false,
        title: Container(
          child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
            Image(image: AssetImage('assets/homeImg/icon1.png')),
            // SizedBox(width: 10,),
            Text(
              'Danh mục',
              style:
                  TextStyle(color: Colors.black, fontWeight: FontWeight.w600),
            ),
            IconButton(
                onPressed: () => {},
                icon: Icon(
                  Icons.keyboard_arrow_down_rounded,
                  color: Colors.black,
                ))
          ]),
        ),
        actions: [
          IconButton(
              onPressed: () => Navigator.of(context)
                  .push(MaterialPageRoute(builder: (_) => SearchDrink())),
              icon: Icon(
                Icons.search,
                color: Colors.grey[700],
              )),
          IconButton(
              onPressed: () => {},
              icon: FaIcon(
                FontAwesomeIcons.heart,
                color: Colors.grey[700],
              )),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Collect(),
            BannerDiscount(),
            ListDrinks(),
          ],
        ),
      ),
      bottomNavigationBar: BottomAppBar(
          child: Container(
              height: 50,
              margin: EdgeInsets.all(20),
              child: GestureDetector(
                onTap: () => {
                  showModalBottomSheet(
                      // isScrollControlled: true,
                      context: context,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      builder: (context) {
                        return FractionallySizedBox(
                          heightFactor: 0.5,
                          child: Container(
                            // color: Colors.white60,
                            padding: EdgeInsets.symmetric(horizontal: 20),
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'Chọn phương thức đặt hàng',
                                      style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    SizedBox(
                                      width: 20,
                                    ),
                                    IconButton(
                                        onPressed: () => Navigator.pop(context),
                                        icon: FaIcon(
                                          FontAwesomeIcons.times,
                                          color: Colors.black,
                                        )),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Image(
                                      image: AssetImage(
                                          'assets/orderImg/icon3.png'),
                                      width: 30,
                                      height: 30,
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          'Giao tận nơi',
                                          style: TextStyle(fontSize: 16),
                                        ),
                                        Text(
                                          'Sản phẩm được giao tận nhà',
                                          style: TextStyle(fontSize: 15),
                                        ),
                                      ],
                                    ),
                                    TextButton(
                                      onPressed: () => {},
                                      child: Text(
                                        'Sửa',
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      style: TextButton.styleFrom(
                                        primary: Colors.white,
                                        backgroundColor: Colors.orange,
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(30)),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Image(
                                      image: AssetImage(
                                          'assets/orderImg/icon4.png'),
                                      width: 30,
                                      height: 30,
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          'Tự đến lấy',
                                          style: TextStyle(fontSize: 16),
                                        ),
                                        Text(
                                          'Bạn sẽ đến tận cửa hàng nhận',
                                          style: TextStyle(fontSize: 15),
                                        ),
                                      ],
                                    ),
                                    TextButton(
                                      onPressed: () => {},
                                      child: Text(
                                        'Sửa',
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      style: TextButton.styleFrom(
                                        primary: Colors.white,
                                        backgroundColor: Colors.orange,
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(30)),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        );
                      })
                },
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Image(
                          image: AssetImage('assets/orderImg/icon1.png'),
                          width: 27,
                        ),
                        Text(
                          'Giao đến',
                          style: TextStyle(
                              color: Colors.grey.shade700,
                              fontSize: 16,
                              fontWeight: FontWeight.w500),
                        ),
                        Icon(
                          Icons.keyboard_arrow_up_rounded,
                          color: Colors.grey.shade700,
                        )
                      ],
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      'Sản phẩm sẽ được gửi đến địa chỉ của bạn',
                      style: TextStyle(
                          color: Colors.grey.shade700,
                          fontSize: 16,
                          fontWeight: FontWeight.w500),
                    ),
                  ],
                ),
              ))),
    );
  }
}
