import 'package:flutter/material.dart';
import 'package:custom_switch/custom_switch.dart';

class Payment extends StatefulWidget {
  static const route = '/payment';
  @override
  _PaymentState createState() => _PaymentState();
}

class _PaymentState extends State<Payment> {
  bool status = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: AppBar().preferredSize,
        child: SafeArea(
          child: Container(
            color: Colors.grey[400],
            child: AppBar(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(25.0),
                      topRight: Radius.circular(25.0))),
              elevation: 0,
              backgroundColor: Colors.white,
              leading: ElevatedButton(
                onPressed: () => Navigator.of(context).pop(),
                child: Icon(Icons.clear),
                style: ElevatedButton.styleFrom(
                    elevation: 0,
                    primary: Colors.white,
                    onPrimary: Colors.black),
              ),
            ),
          ),
        ),
      ),
      body: Container(
        color: Colors.grey[100],
        child: Stack(
          children: [
            SingleChildScrollView(
              child: Container(
                color: Colors.white,
                padding: EdgeInsets.all(20),
                margin: EdgeInsets.only(top: 20, bottom: 20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Giao tận nơi',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 28),
                        ),
                        TextButton(
                          onPressed: () => {},
                          child: Text(
                            'Thay đổi',
                            style: TextStyle(
                                fontSize: 15,
                                fontWeight: FontWeight.bold,
                                color: Colors.orange.shade800),
                          ),
                          style: TextButton.styleFrom(
                            primary: Colors.white,
                            backgroundColor: Colors.amber.shade50,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30)),
                            minimumSize: Size(80, 30),
                          ),
                        ),
                      ],
                    ),
                    ListTile(
                      title: Text(
                        '88 Cao Thắng',
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                      subtitle: Text(
                        '88 Cao Thắng, Hồ Chí Minh',
                        style: TextStyle(fontSize: 15),
                      ),
                      trailing: Icon(Icons.keyboard_arrow_right),
                    ),
                    TextField(
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'Thêm hướng dẫn giao hàng',
                      ),
                    ),
                    Divider(),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text(
                              'Tên người dùng',
                              style: TextStyle(fontSize: 20),
                            ),
                            Text(
                              'Số điện thoại',
                              style: TextStyle(
                                  fontSize: 15, color: Colors.grey.shade900),
                            ),
                          ],
                        ),
                        VerticalDivider(),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text(
                              '15-30 phút',
                              style: TextStyle(fontSize: 20),
                            ),
                            Text(
                              'Càng sớm càng tốt',
                              style: TextStyle(
                                  fontSize: 15, color: Colors.grey.shade900),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Divider(),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Text(
                            'Lưu thông tin giao hàng cho lần sau',
                            style: TextStyle(fontSize: 20),
                          ),
                        ),
                        CustomSwitch(
                          activeColor: Colors.orange,
                          value: status,
                          onChanged: (value) {
                            print("VALUE : $value");
                            setState(() {
                              status = value;
                            });
                          },
                        )
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    SizedBox(
                      height: 20,
                      child: Container(
                        color: Colors.grey.shade100,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Text(
                            'Các sản phẩm đã chọn',
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 28),
                          ),
                        ),
                        TextButton(
                          onPressed: () => {},
                          child: Text(
                            '+ Thêm',
                            style: TextStyle(
                                fontSize: 15,
                                fontWeight: FontWeight.bold,
                                color: Colors.orange.shade800),
                          ),
                          style: TextButton.styleFrom(
                            primary: Colors.white,
                            backgroundColor: Colors.amber.shade50,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30)),
                            minimumSize: Size(80, 30),
                          ),
                        ),
                      ],
                    ),
                    ListTile(
                      leading: Icon(Icons.edit),
                      title: Text(
                        'Sản phẩm bạn chọn',
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                      subtitle: Text(
                        'Loại Size',
                        style: TextStyle(
                            fontSize: 18, color: Colors.grey.shade800),
                      ),
                      trailing: Text(
                        '35.000đ',
                        style: TextStyle(
                          fontSize: 18,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                      child: Container(
                        color: Colors.grey.shade100,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      'Tổng cộng',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 28),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Thành tiền',
                          style: TextStyle(
                            fontSize: 20,
                          ),
                        ),
                        Text(
                          '35,000đ',
                          style: TextStyle(
                            fontSize: 20,
                          ),
                        ),
                      ],
                    ),
                    Divider(),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Phí vận chuyển',
                          style: TextStyle(
                            fontSize: 20,
                          ),
                        ),
                        Text(
                          '10,000đ',
                          style: TextStyle(
                            fontSize: 20,
                          ),
                        ),
                      ],
                    ),
                    Divider(),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Chọn khuyến mãi',
                          style:
                              TextStyle(fontSize: 20, color: Colors.blueAccent),
                        ),
                        Icon(Icons.keyboard_arrow_right)
                      ],
                    ),
                    Divider(),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Số tiền thanh toán',
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold),
                        ),
                        Text(
                          '45,000đ',
                          style: TextStyle(
                            fontSize: 20,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    SizedBox(
                      height: 20,
                      child: Container(
                        color: Colors.grey.shade100,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      'Thanh toán',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 28),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Text(
                            'Bấm để chọn phương thức thanh toán',
                            style: TextStyle(
                                fontSize: 20, color: Colors.blueAccent),
                          ),
                        ),
                        Icon(Icons.keyboard_arrow_right)
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    SizedBox(
                      height: 20,
                      child: Container(
                        color: Colors.grey.shade100,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Icon(
                          Icons.delete,
                          color: Colors.red,
                        ),
                        Expanded(
                          child: Text(
                            'Xóa đơn hàng đã chọn',
                            style: TextStyle(fontSize: 20, color: Colors.red),
                          ),
                        ),
                        SizedBox(
                          height: 100,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              bottom: 0,
              child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: 100,
                  padding: EdgeInsets.all(20),
                  color: Colors.orange.shade800,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Column(
                        children: [
                          Text(
                            'Giao tận nơi',
                            style: TextStyle(fontSize: 20, color: Colors.white),
                          ),
                          Text(
                            '45.000đ',
                            style: TextStyle(
                                fontSize: 20,
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                      TextButton(
                        onPressed: () => {},
                        child: Text(
                          'Đặt hàng',
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold),
                        ),
                        style: TextButton.styleFrom(
                          primary: Colors.orange.shade800,
                          backgroundColor: Colors.white,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30)),
                          minimumSize: Size(100, 50),
                        ),
                      ),
                    ],
                  )),
            ),
          ],
        ),
      ),
    );
  }
}
