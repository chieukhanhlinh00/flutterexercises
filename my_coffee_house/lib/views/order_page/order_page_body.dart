import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

import '../../models/data_order.dart';
import '../voucher_page/tickets_voucher/voucher.dart';

class Collect extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.all(10),
      child: GridView.builder(
        shrinkWrap: true,
        itemCount: imgListCollect.length,
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 4,
          mainAxisExtent: 150,
        ),
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            onTap: () {},
            child: Column(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(60),
                  child: Image(
                    image: AssetImage(imgListCollect[index]),
                    width: 70,
                    height: 70,
                  ),
                ),
                SizedBox(
                  height: 8,
                ),
                Container(
                    width: 100,
                    child: Column(
                      children: [
                        Text(textListCollect[index],
                            style: TextStyle(
                                fontSize: 18,
                                color: Colors.grey.shade700,
                                fontWeight: FontWeight.w500),
                            textAlign: TextAlign.center),
                        SizedBox(
                          height: 5,
                        ),
                      ],
                    )),
              ],
            ),
          );
        },
      ),
    );
  }
}

class BannerDiscount extends StatefulWidget {
  const BannerDiscount({Key? key}) : super(key: key);
  State<BannerDiscount> createState() => _BannerDiscountState();
}

class _BannerDiscountState extends State<BannerDiscount> {
  final imgDisc = [
    'assets/homeImg/banner7.png',
    'assets/homeImg/banner8.png',
  ];
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.amber[50],
      padding: EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Row(
              children: [
                Image(
                  image: AssetImage('assets/orderImg/icon2.png'),
                  width: 19,
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  'Bộ sưu tập',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 25,
                      fontWeight: FontWeight.bold),
                )
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          GestureDetector(
            onTap: () => {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (_) => CurVoucher())),
            },
            child: CarouselSlider.builder(
              itemCount: imgDisc.length,
              options: CarouselOptions(
                height: 190,
                enlargeCenterPage: false,
                autoPlay: true,
                aspectRatio: 16 / 7,
                autoPlayCurve: Curves.fastOutSlowIn,
                enableInfiniteScroll: false,
                autoPlayAnimationDuration: Duration(milliseconds: 800),
                viewportFraction: 0.9,
              ),
              itemBuilder: (BuildContext context, int index, int realIndex) {
                final _imgDisc = imgDisc[index];

                return buildImageDisc(_imgDisc, index);
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget buildImageDisc(String imgDisc, int index) => Container(
      margin: EdgeInsets.symmetric(horizontal: 0),
      child: Container(
        margin: EdgeInsets.all(5),
        height: 100.0,
        width: 500.0,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          //set border radius to 50% of square height and width
          image: DecorationImage(
            image: AssetImage(imgDisc),
            fit: BoxFit.cover, //change image fill type
          ),
        ),
      ));
}
