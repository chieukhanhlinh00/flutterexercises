import 'package:flutter/material.dart';
import 'list_drinks/coffee.dart';
import 'list_drinks/enjoy_at_home.dart';
import 'list_drinks/fruit_tea.dart';
import 'list_drinks/hi_tea.dart';
import 'list_drinks/ice_drink.dart';
import 'list_drinks/snack.dart';

class ListDrinks extends StatefulWidget{
  @override
  _ListDrinksState createState() => _ListDrinksState();
}

class _ListDrinksState extends State<ListDrinks>{
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.all(20),
      child: Column(
        children: [
          Row(
            children: [
              Image(image: AssetImage('assets/orderImg/col1.png'), width: 30,),
              SizedBox(width: 10,),
              Text('Cà phê',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 25,
                    fontWeight: FontWeight.bold),)
            ],
          ),
          SizedBox(height: 10,),
          Coffee(),
          Row(
            children: [
              Image(image: AssetImage('assets/orderImg/col2.png'), width: 30),
              SizedBox(width: 10,),
              Text('Trà Trái Cây',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 25,
                    fontWeight: FontWeight.bold),)
            ],
          ),
          SizedBox(height: 10,),
          FruitTea(),
          Row(
            children: [
              Image(image: AssetImage('assets/orderImg/col3.png'), width: 30),
              SizedBox(width: 10,),
              Text('Hi-Tea Healthy',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 25,
                    fontWeight: FontWeight.bold),)
            ],
          ),
          SizedBox(height: 10,),
          HiTea(),
          Row(
            children: [
              Image(image: AssetImage('assets/orderImg/col4.png'), width: 30),
              SizedBox(width: 10,),
              Text('Đá Xay - Choco',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 25,
                    fontWeight: FontWeight.bold),)
            ],
          ),
          SizedBox(height: 10,),
          IceDrink(),
          Row(
            children: [
              Image(image: AssetImage('assets/orderImg/col5.png'), width: 30),
              SizedBox(width: 10,),
              Text('Thưởng thức tại nhà',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 25,
                    fontWeight: FontWeight.bold),)
            ],
          ),
          SizedBox(height: 10,),
          EnjoyHome(),
          Row(
            children: [
              Image(image: AssetImage('assets/orderImg/col6.png'), width: 30),
              SizedBox(width: 10,),
              Text('Bánh - Snack',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 25,
                    fontWeight: FontWeight.bold),)
            ],
          ),
          SizedBox(height: 10,),
          Snack(),
        ],
      ),
    );
  }

}







