import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:group_button/group_button.dart';

import '../../../models/data_order.dart';
import '../payment/payment_page.dart';

class FruitTea extends StatefulWidget {
  @override
  _FruitTeaState createState() => _FruitTeaState();
}

class _FruitTeaState extends State<FruitTea> {
  String? choice;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 20),
      child: ListView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemCount: titleListFruitTea.length,
        scrollDirection: Axis.vertical,
        itemBuilder: (context, index) => Container(
          margin: EdgeInsets.all(5),
          color: Colors.white,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Row(
                // mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Image(
                    image: AssetImage(listImgFruitTea[index]),
                    width: 100,
                    height: 100,
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Column(
                    // mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 20,
                      ),
                      SizedBox(
                        width: 150,
                        child: Text(
                          titleListFruitTea[index],
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                          softWrap: false,
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        listFruitTeaPrice[index],
                        style: TextStyle(fontSize: 18),
                      )
                    ],
                  ),
                ],
              ),
              Container(
                width: 40,
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey, width: 0.5),
                  color: Colors.orange,
                  shape: BoxShape.circle,
                ),
                child: IconButton(
                  color: Colors.white,
                  iconSize: 20,
                  icon: FaIcon(FontAwesomeIcons.plus),
                  onPressed: () {
                    showModalBottomSheet(
                        isScrollControlled: true,
                        context: context,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        builder: (context) {
                          return FractionallySizedBox(
                              heightFactor: 0.9,
                              child: Stack(
                                children: [
                                  SingleChildScrollView(
                                    child: Container(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Image(
                                            image: AssetImage(
                                                listImgFruitTeadt[index]),
                                            width: MediaQuery.of(context)
                                                .size
                                                .width,
                                          ),
                                          SizedBox(
                                            height: 20,
                                          ),
                                          Container(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 20),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    Text(
                                                      titleListCoffee[index],
                                                      style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontSize: 30),
                                                    ),
                                                    FaIcon(
                                                        FontAwesomeIcons.heart)
                                                  ],
                                                ),
                                                Text(
                                                  listCoffeePrice[index],
                                                  style:
                                                      TextStyle(fontSize: 20),
                                                ),
                                                SizedBox(
                                                  height: 10,
                                                ),
                                                SizedBox(
                                                  height: 10,
                                                  child: Container(
                                                    color: Colors.grey.shade100,
                                                  ),
                                                ),
                                                SizedBox(
                                                  height: 10,
                                                ),
                                                Text(
                                                  'Size*',
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 30),
                                                ),
                                                Text(
                                                  'Chọn 1 loại size',
                                                  style: TextStyle(
                                                      fontSize: 20,
                                                      color:
                                                          Colors.grey.shade700),
                                                ),
                                                Divider(),
                                                Column(
                                                  children: [
                                                    ListTile(
                                                      title: Text(
                                                        "Lớn",
                                                        style: TextStyle(
                                                          fontSize: 20,
                                                        ),
                                                      ),
                                                      leading: Radio(
                                                          value: "big",
                                                          groupValue: choice,
                                                          onChanged: (value) {
                                                            setState(() {
                                                              choice = value
                                                                  .toString();
                                                            });
                                                          }),
                                                      trailing: Text(
                                                        '+4,000đ',
                                                        style: TextStyle(
                                                          fontSize: 20,
                                                        ),
                                                      ),
                                                    ),
                                                    ListTile(
                                                      title: Text(
                                                        "Vừa",
                                                        style: TextStyle(
                                                          fontSize: 20,
                                                        ),
                                                      ),
                                                      leading: Radio(
                                                          value: "Med",
                                                          groupValue: choice,
                                                          onChanged: (value) {
                                                            setState(() {
                                                              choice = value
                                                                  .toString();
                                                            });
                                                          }),
                                                      trailing: Text(
                                                        '+0đ',
                                                        style: TextStyle(
                                                          fontSize: 20,
                                                        ),
                                                      ),
                                                    ),
                                                    ListTile(
                                                      title: Text(
                                                        "Nhỏ",
                                                        style: TextStyle(
                                                          fontSize: 20,
                                                        ),
                                                      ),
                                                      leading: Radio(
                                                          value: "Small",
                                                          groupValue: choice,
                                                          onChanged: (value) {
                                                            setState(() {
                                                              choice = value
                                                                  .toString();
                                                            });
                                                          }),
                                                      trailing: Text(
                                                        '-4,000đ',
                                                        style: TextStyle(
                                                          fontSize: 20,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                SizedBox(
                                                  height: 10,
                                                ),
                                                SizedBox(
                                                  height: 10,
                                                  child: Container(
                                                    color: Colors.grey.shade100,
                                                  ),
                                                ),
                                                SizedBox(
                                                  height: 10,
                                                ),
                                                Text(
                                                  'Topping',
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 30),
                                                ),
                                                Text(
                                                  'Chọn 2 loại',
                                                  style: TextStyle(
                                                      fontSize: 20,
                                                      color:
                                                          Colors.grey.shade700),
                                                ),
                                                Divider(),
                                                Center(
                                                  child: GroupButton(
                                                    isRadio: false,
                                                    buttons: [
                                                      "Phô Mai",
                                                      "Đào Miếng",
                                                      "Trái Vải"
                                                    ],
                                                  ),
                                                ),
                                                SizedBox(
                                                  height: 10,
                                                ),
                                                SizedBox(
                                                  height: 10,
                                                  child: Container(
                                                    color: Colors.grey.shade100,
                                                  ),
                                                ),
                                                SizedBox(
                                                  height: 10,
                                                ),
                                                Text(
                                                  'Yêu cầu khác',
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 30),
                                                ),
                                                Text(
                                                  'Những tùy chọn khác',
                                                  style: TextStyle(
                                                      fontSize: 20,
                                                      color:
                                                          Colors.grey.shade700),
                                                ),
                                                Divider(),
                                                Container(
                                                    width: double.infinity,
                                                    height: 50,
                                                    padding: const EdgeInsets
                                                            .symmetric(
                                                        vertical: 2,
                                                        horizontal: 20),
                                                    decoration: BoxDecoration(
                                                      color: Colors.black38
                                                          .withAlpha(10),
                                                      borderRadius:
                                                          BorderRadius.all(
                                                        Radius.circular(10),
                                                      ),
                                                    ),
                                                    child: Row(
                                                      children: [
                                                        Expanded(
                                                          child: TextField(
                                                            decoration:
                                                                InputDecoration(
                                                              hintText:
                                                                  "Thêm ghi chú",
                                                              hintStyle: TextStyle(
                                                                  color: Colors
                                                                      .black54,
                                                                  fontSize: 18),
                                                              border:
                                                                  InputBorder
                                                                      .none,
                                                            ),
                                                            onChanged: (String
                                                                keyword) {},
                                                          ),
                                                        ),
                                                      ],
                                                    )),
                                                SizedBox(
                                                  height: 150,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Align(
                                    child: IconButton(
                                        onPressed: () => Navigator.pop(context),
                                        icon: FaIcon(
                                          FontAwesomeIcons.timesCircle,
                                          size: 30,
                                          color: Colors.grey.shade600,
                                        )),
                                    alignment: Alignment.topRight,
                                  ),
                                  Align(
                                    alignment: Alignment.bottomCenter,
                                    child: Container(
                                      color: Colors.white,
                                      padding:
                                          EdgeInsets.symmetric(horizontal: 20),
                                      height: 100,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Container(
                                            width: 40,
                                            decoration: BoxDecoration(
                                              color: Colors.amber.shade100,
                                              shape: BoxShape.circle,
                                            ),
                                            child: IconButton(
                                              // color: Colors.black,
                                              iconSize: 20,
                                              icon: FaIcon(
                                                FontAwesomeIcons.plus,
                                                color: Colors.orange,
                                              ),
                                              onPressed: () {},
                                            ),
                                          ),
                                          Text(
                                            '1',
                                            style: TextStyle(fontSize: 20),
                                          ),
                                          Container(
                                            width: 40,
                                            decoration: BoxDecoration(
                                              color: Colors.amber.shade100,
                                              shape: BoxShape.circle,
                                            ),
                                            child: IconButton(
                                              // color: Colors.black,
                                              iconSize: 20,
                                              icon: FaIcon(
                                                FontAwesomeIcons.minus,
                                                color: Colors.orange,
                                              ),
                                              onPressed: () {},
                                            ),
                                          ),
                                          TextButton(
                                            onPressed: () =>
                                                Navigator.of(context).push(
                                                    MaterialPageRoute(
                                                        builder: (_) =>
                                                            Payment())),
                                            child: Text(
                                              'Mua Hàng',
                                              style: TextStyle(
                                                  fontSize: 20,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            style: TextButton.styleFrom(
                                              primary: Colors.white,
                                              backgroundColor:
                                                  Colors.orange.shade800,
                                              minimumSize: Size(200, 50),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  )
                                ],
                              ));
                        });
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
