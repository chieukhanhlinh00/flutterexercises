
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../validations/mixins_validation.dart';
// import '../home_page/home_page.dart';

class Login extends StatefulWidget{
  static const route = '/login';
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> with CommonValidation {
  final formKey = GlobalKey<FormState>();
  late String phone;
  @override
  Widget build(BuildContext context) {
    return Material(
        child: Stack(
          children: [
            Align(
                alignment: Alignment.topCenter,
                child:Image(image: AssetImage('assets/loginImg/banner1.jpg'),
                  width: MediaQuery.of(context).size.width,)
            ),

            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                height: 700,
                padding: EdgeInsets.symmetric(horizontal: 20),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30.0),
                        topRight: Radius.circular(30.0)
                    )
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text('Chào mừng bạn đến với',style: TextStyle(
                        color: Colors.grey.shade600,
                        fontSize: 20
                    ),),
                    Text('MY COFFEE HOUSE',style: TextStyle(
                        color: Colors.black,
                        fontSize: 30, fontWeight: FontWeight.bold
                    ),),

                    TextFormField(
                      keyboardType: TextInputType.phone,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: "Nhập số điện thoại"
                      ),
                    ),

                    TextButton(
                      onPressed: () => Navigator.popUntil(context, ModalRoute.withName('/')),
                      child: Text('Đăng nhập', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
                      style: TextButton.styleFrom(
                        primary: Colors.white,
                        backgroundColor: Colors.orange.shade800,
                        minimumSize: Size(450, 60),
                      ),
                    ),

                    Divider(),

                  TextButton(onPressed: ()=>{},
                      child: Text('Tiếp tục bằng Facebook', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
                    style: TextButton.styleFrom(
                      primary: Colors.white,
                      backgroundColor: Colors.blueAccent,
                      minimumSize: Size(450, 60),
                    ),),

                    TextButton(onPressed: ()=>{},
                      child: Text('Tiếp tục bằng Google', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
                      style: TextButton.styleFrom(
                        primary: Colors.black,
                        backgroundColor: Colors.white,
                        side: BorderSide(color: Colors.grey, width: 0.5),
                        minimumSize: Size(450, 60),
                      ),),

                  ],
                ),
              ),
            ),
            Align(
              child: IconButton(onPressed: ()=> Navigator.popUntil(context, ModalRoute.withName('/')),
                  icon: FaIcon(FontAwesomeIcons.timesCircle, size: 30 , color: Colors.black,)),
              alignment: Alignment.topLeft,
            ),
          ],
        ),
    );

  }

}