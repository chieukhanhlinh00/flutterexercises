import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:my_coffee_house/views/search_page/search_shop.dart';

import '../login_page/login_page.dart';
import '../voucher_page/tickets_voucher/ticket.dart';
import 'shops.dart';

class OurshopPage extends StatefulWidget {
  static const route = '/ourshop';
  State<OurshopPage> createState() => _OurshopPageState();
}

class _OurshopPageState extends State<OurshopPage> {
  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(120),
        child: AppBar(
          elevation: 0,
          backgroundColor: Colors.white,
          title: Text(
            'Cửa hàng',
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.w600),
          ),
          actions: [
            Container(
              width: 40,
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey, width: 0.5),
                  color: Colors.white,
                  shape: BoxShape.circle,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.7),
                      spreadRadius: 1,
                      blurRadius: 3,
                    )
                  ]),
              child: IconButton(
                color: Colors.black,
                iconSize: 20,
                icon: FaIcon(FontAwesomeIcons.ticketAlt),
                onPressed: () => Navigator.of(context)
                    .push(MaterialPageRoute(builder: (_) => Ticket())),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 8, right: 8),
              width: 40,
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey, width: 0.5),
                  color: Colors.white,
                  shape: BoxShape.circle,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.7),
                      spreadRadius: 1,
                      blurRadius: 3,
                    )
                  ]),
              child: IconButton(
                color: Colors.black,
                iconSize: 20,
                icon: Icon(Icons.notifications),
                onPressed: () {
                  Navigator.of(context).pushNamed(Login.route);
                },
              ),
            ),
          ],
          bottom: AppBar(
            elevation: 0,
            backgroundColor: Colors.white,
            title: GestureDetector(
              onTap: () => Navigator.of(context)
                  .push(MaterialPageRoute(builder: (_) => SearchShop())),
              child: Container(
                width: double.infinity,
                height: 50,
                padding:
                    const EdgeInsets.symmetric(vertical: 2, horizontal: 20),
                decoration: BoxDecoration(
                  color: Colors.black38.withAlpha(10),
                  borderRadius: BorderRadius.all(
                    Radius.circular(10),
                  ),
                ),
                child: Row(children: <Widget>[
                  Icon(
                    Icons.search,
                    color: Colors.black.withAlpha(120),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Expanded(
                    child: TextField(
                      decoration: InputDecoration(
                        hintText: "Nhập tên đường, quận huyện...",
                        hintStyle:
                            TextStyle(color: Colors.black54, fontSize: 18),
                        border: InputBorder.none,
                      ),
                      onChanged: (String keyword) {},
                    ),
                  ),
                ]),
              ),
            ),
            actions: [
              TextButton.icon(
                  onPressed: () => {},
                  icon: Icon(
                    Icons.map_outlined,
                    size: 17,
                    color: Colors.black,
                  ),
                  label: Text(
                    'Bản đồ',
                    style: TextStyle(color: Colors.black, fontSize: 17),
                  ))
            ],
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          color: Colors.grey[100],
          padding: EdgeInsets.all(20),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Text(
              'Các cửa hàng khác',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 20,
            ),
            Shops(),
          ]),
        ),
      ),
    );
  }
}
