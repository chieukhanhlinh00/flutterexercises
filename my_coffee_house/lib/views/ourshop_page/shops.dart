import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../models/data_shop.dart';

class Shops extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemCount: titleListShop.length,
        scrollDirection: Axis.vertical,
        itemBuilder: (context, index) => Container(
          child: Card(
            margin: EdgeInsets.only(bottom: 10),
            elevation: 0.0,
            child: Container(
              padding: EdgeInsets.all(20),
              child: GestureDetector(
                onTap: () => {
                  showModalBottomSheet(
                      isScrollControlled: true,
                      context: context,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)
                      ),
                      builder: (context){
                        return FractionallySizedBox(
                            heightFactor: 0.9,
                          child: Stack(
                            children: [
                              SingleChildScrollView(
                                //     controller: scrollController,
                                  child: Container(
                                    // margin: EdgeInsets.only(top: 20),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.stretch,
                                      children: [
                                        Image(image: AssetImage('assets/shopImg/bg1.jpg'),
                                          width: MediaQuery.of(context).size.width,
                                        ),
                                        SizedBox(height: 20,),

                                        Container(
                                          margin: EdgeInsets.only(left: 20),
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text('THE COFFEE HOUSE', style: TextStyle(fontSize: 13, color: Colors.black54, fontWeight: FontWeight.bold),),
                                              Text(titleListShop[index], style: TextStyle(fontSize: 25, color: Colors.black, fontWeight: FontWeight.bold),),
                                              Text('Giờ mở cửa: 7:00-21:30', style: TextStyle(fontSize: 18, color: Colors.black54,),),

                                            ],
                                          ),
                                        ),
                                        Container(
                                          child: Column(
                                            children: [
                                              Row(
                                                children: [
                                                  Container(
                                                        margin: EdgeInsets.only(left: 20, right: 20),
                                                        child: Ink(
                                                          decoration: ShapeDecoration(
                                                              color: Colors.grey.shade200,
                                                              shape: RoundedRectangleBorder(
                                                                borderRadius: BorderRadius.circular(10),
                                                              )
                                                          ),
                                                          child: IconButton(onPressed: ()=> {},
                                                            icon: Icon(Icons.near_me_outlined ),
                                                          ),
                                                        ),
                                                      ),


                                                Expanded(
                                                  child:Container(
                                                    height: 50,
                                                    margin: EdgeInsets.all(20),
                                                    padding: EdgeInsets.only(top: 10),
                                                    decoration: BoxDecoration(
                                                        border: Border(bottom: BorderSide(width: 1.0, color: Colors.grey.shade300),)
                                                    ),
                                                    child: Text(titleListShop[index] + ', Hồ Chí Minh, Việt Nam',
                                                      style: TextStyle(fontSize: 18),
                                                      textAlign: TextAlign.center,
                                                    ),
                                                  ),
                                                  ),

                                                ],
                                              )
                                            ],
                                          ),
                                        ),
                                        Container(
                                          child: Column(
                                            children: [
                                              Row(
                                                children: [
                                                  Container(
                                                    margin: EdgeInsets.only(left: 20),
                                                    child: Ink(
                                                      decoration: ShapeDecoration(
                                                          color: Colors.grey.shade200,
                                                          shape: RoundedRectangleBorder(
                                                            borderRadius: BorderRadius.circular(10),
                                                          )
                                                      ),
                                                      child: IconButton(onPressed: ()=> {},
                                                        icon: Icon(Icons.favorite_border_rounded ),
                                                      ),
                                                    ),
                                                  ),

                                                Expanded(
                                                  child:Container(
                                                    height: 50,
                                                    margin: EdgeInsets.all(20),
                                                    padding: EdgeInsets.only(top: 10),
                                                    decoration: BoxDecoration(
                                                        border: Border(bottom: BorderSide(width: 1.0, color: Colors.grey.shade300),)
                                                    ),
                                                    child: Text('Thêm vào danh sách yêu thích',
                                                      style: TextStyle(fontSize: 18),
                                                      textAlign: TextAlign.center,
                                                    ),
                                                  ),
                                                  )

                                                ],
                                              )
                                            ],
                                          ),
                                        ),
                                        Container(
                                          child: Column(
                                            children: [
                                              Row(
                                                children: [
                                                  Container(
                                                    margin: EdgeInsets.only(left: 20),

                                                    child: Ink(
                                                      decoration: ShapeDecoration(
                                                          color: Colors.grey.shade200,
                                                          shape: RoundedRectangleBorder(
                                                            borderRadius: BorderRadius.circular(10),
                                                          )
                                                      ),
                                                      child: IconButton(onPressed: ()=> {},
                                                        icon: Icon(Icons.ios_share_rounded ),
                                                      ),
                                                    ),
                                                  ),

                                                Expanded(
                                                  child: Container(
                                                    height: 50,
                                                    margin: EdgeInsets.all(20),
                                                    padding: EdgeInsets.only(top: 10),
                                                    decoration: BoxDecoration(
                                                        border: Border(bottom: BorderSide(width: 1.0, color: Colors.grey.shade300),)
                                                    ),
                                                    child: Text('Chia sẻ địa chỉ',
                                                      style: TextStyle(fontSize: 18),
                                                      textAlign: TextAlign.center,
                                                    ),
                                                  ),
                                                  ),

                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                        Container(
                                          child: Column(
                                            children: [
                                              Row(
                                                children: [
                                                  Container(
                                                    margin: EdgeInsets.only(left: 20),

                                                    child: Ink(
                                                      decoration: ShapeDecoration(
                                                          color: Colors.grey.shade200,
                                                          shape: RoundedRectangleBorder(
                                                            borderRadius: BorderRadius.circular(10),
                                                          )
                                                      ),
                                                      child: IconButton(onPressed: ()=> {},
                                                        icon: Icon(Icons.local_phone_outlined ),
                                                      ),
                                                    ),
                                                  ),

                                                Expanded(
                                                  child: Container(
                                                    height: 50,
                                                    margin: EdgeInsets.all(20),
                                                    padding: EdgeInsets.only(top: 10),
                                                    decoration: BoxDecoration(
                                                        border: Border(bottom: BorderSide(width: 1.0, color: Colors.grey.shade300),)
                                                    ),
                                                    child: Text('liên hệ : 18006936',
                                                      style: TextStyle(fontSize: 18),
                                                      textAlign: TextAlign.center,
                                                    ),
                                                  ),
                                                  ),

                                                ],
                                              ),
                                              SizedBox(height: 20,),
                                              ClipRRect(
                                                borderRadius: BorderRadius.circular(10.0), //add border radius
                                                child: Image.asset(
                                                  "assets/shopImg/map.png",
                                                  height: 400.0,
                                                  width: 500.0,
                                                  fit:BoxFit.cover,
                                                ),
                                              )
                                            ],
                                          ),
                                        )

                                      ],
                                    ),
                                  )
                              ),
                              
                              Align(
                                  child: IconButton(onPressed: ()=> Navigator.pop(context),
                                      icon: FaIcon(FontAwesomeIcons.timesCircle, size: 30 , color: Colors.grey.shade600,)),
                                alignment: Alignment.topRight,
                              )

                            ],
                          ),
                          
                        );
                      })
                },
              child: ListTile(

                title: Text('THE COFFEE HOUSE', style: TextStyle(fontSize: 13, color: Colors.black54, fontWeight: FontWeight.bold),),
                subtitle: Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(titleListShop[index], style: TextStyle(fontSize: 18, color: Colors.black),),
                                Text(listAdd[index], style: TextStyle(color: Colors.black54, fontSize: 18),)
                              ],
                            ),
                ),
                leading: Container( height: 70, width: 70,
                  decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  image: DecorationImage(image: AssetImage(picListShop[index]),fit: BoxFit.cover),
                ),),
              ),
              ),
            ),
          ),
        ),
    );
  }

}