import 'package:flutter/material.dart';

//grid 1 - Ưu đãi
List gridItemsImg1=[
  'assets/homeImg/grid1_1.png',
  'assets/homeImg/grid1_2.png',
  'assets/homeImg/grid1_3.png',
  'assets/homeImg/grid1_4.png',
  'assets/homeImg/grid1_5.png',
  'assets/homeImg/grid1_6.png',
];

List gridItemsText1=[
  'Nước thiệt chill - deal tiết kiệm, nhà & shopeepay mời bạn ngay 50%',
  'Thương mẹ nhiều - nhà mời deal 35%',
  'Hi-Tea Healthy-0đ đúng ý',
  'Hi-Tea Healthy: Upsize miễn phí',
  'Hè hết ý - deal mê ly',
  'Sau lưng bạn có deal 35% kìa',
];

List gridItemsDate1=[
  '09/05',
  '06/05',
  '04/05',
  '03/05',
  '01/05',
  '27/04',
];

// grid 2 - Cập nhật từ nhà

List gridItemsImg2=[
  'assets/homeImg/grid2_1.png',
  'assets/homeImg/grid2_2.png',
  'assets/homeImg/grid2_3.png',
  'assets/homeImg/grid2_4.png',
  'assets/homeImg/grid2_5.png',
  'assets/homeImg/grid2_6.png',
];

List gridItemsText2=[
  'Cập nhật từ Nhà',
  'Cập nhật từ Nhà',
  'Chào mừng nhà mới the park home - cầu giấy - hà nội',
  'Chỉ còn 3 ngày đổi bean ngay!',
  'Photo contest "nhà là nơi...',
  'Dọn bean năm cũ - đón khởi đầu sung',
];

List gridItemsDate2=[
  '21/04',
  '08/04',
  '10/03',
  '26/02',
  '20/02',
  '15/02',
];
// grid 3 - coffee lover

List gridItemsImg3=[
  'assets/homeImg/grid3_1.png',
  'assets/homeImg/grid3_2.png',
  'assets/homeImg/grid3_3.png',
  'assets/homeImg/grid3_4.png',
  'assets/homeImg/grid3_5.png',
  'assets/homeImg/grid3_6.png',
];

List gridItemsText3=[
  '#CoffêLover',
  'Nghệ thuật pha chế - Kalita Wave',
  'Nghệ thuật pha chế - Cold Brew',
  'Nghệ thuật pha chế - Espresso',
  'Nghệ thuật pha chế - Cappuccino',
  'Nghệ thuật pha chế - Latte',
];

List gridItemsDate3=[
  '29/08',
  '28/08',
  '27/08',
  '26/08',
  '25/08',
  '24/08',
];