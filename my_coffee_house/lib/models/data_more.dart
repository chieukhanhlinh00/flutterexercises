
import 'package:flutter/material.dart';
//Help

List titleListHelp = [
  'Đánh giá đơn hàng',
  'Liên hệ và góp ý'
];
List tittleIconHelp = [
  Icons.star_border_outlined ,
  Icons.chat_bubble_outline ,
];

//Account
List titleListAccount = [
  'Thông tin cá nhân',
  'Địa chỉ đã lưu',
  'Cài đặt',
  'Đăng nhập'
];

List tittleIconAccount = [
  Icons.person_outline ,
  Icons.bookmark_border ,
  Icons.settings_outlined ,
  Icons.login ,
];