//accumulate screen
//card1
List imgList1=[
  'assets/cardImg/C1_1.png'
];

List titleList1= [
  'Miễn phí upsize cho đơn đầu tiên'
];

//card2
List imgList2=[
  'assets/cardImg/C2_1.png',
  'assets/cardImg/C2_2.png',
  'assets/cardImg/C2_3.png',
];

List titleList2= [
  'Tặng 01 phần bánh sinh nhật',
  'Miễn phí 01 phần Snack cho đơn hàng trên 100,000đ',
  'Đặc quyền Đổi Ưu đãi bằng điểm BEAN tích lũy',
];

//card3
List imgList3=[
  'assets/cardImg/C2_1.png',
  'assets/cardImg/C3_2.png',
  'assets/cardImg/C2_3.png',
];

List titleList3= [
  'Tặng 01 phần bánh sinh nhật',
  'Ưu đãi Mua 2 tặng 1',
  'Đặc quyền Đổi Ưu đãi bằng điểm BEAN tích lũy',
];

//card4
List imgList4=[
  'assets/cardImg/C2_1.png',
  'assets/cardImg/C1_1.png',
  'assets/cardImg/C2_3.png',
];

List titleList4= [
  'Tặng 01 phần bánh sinh nhật',
  'Miễn phí 1 phần nước Cà phê/ Trà',
  'Đặc quyền Đổi Ưu đãi bằng điểm BEAN tích lũy',
];

//card5
List imgList5=[
  'assets/cardImg/C5_1.png',
  'assets/cardImg/C2_1.png',
  'assets/cardImg/C1_1.png',
  'assets/cardImg/C2_2.png',
  'assets/cardImg/C5_5.png',
  'assets/cardImg/C2_3.png',
];

List titleList5= [
  'Được nhân 1.5 BEAN tích lũy',
  'Tặng 01 phần bánh sinh nhật',
  'Miễn phí 1 phần nước bất kì',
  'Nhận riêng Ưu đãi từ My Coffee House và đối tác khác',
  'Cơ hội trải nghiệm & hưởng đặc quyền đầu tiên',
  'Đặc quyền Đổi Ưu đãi bằng điểm BEAN tích lũy',
];

//List ticket
List imgTicket=[
  'assets/voucherImg/vc1.png',
  'assets/voucherImg/vc2.png',
  'assets/voucherImg/vc3.png',
];

List titleTicket=[
  'Giảm 50k cho hóa đơn 199k. Nhập mã HOIHE',
  'Giảm 30% tối đa 35k cho đơn từ 4 món',
  'Ưu đãi ly cà phê 19k khi mua cùng bánh mì thịt'
];

List outDateTicket=[
  'Hết hạn 31/05/2022',
  'Hết hạn 30/06/2022',
  'Hết hạn 30/06/2022',

];