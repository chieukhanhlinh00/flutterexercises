List imgListCollect = [
  'assets/orderImg/col1.png',
  'assets/orderImg/col2.png',
  'assets/orderImg/col3.png',
  'assets/orderImg/col4.png',
  'assets/orderImg/col5.png',
  'assets/orderImg/col6.png',
] ;

List textListCollect = [
  'Cà phê',
  'Trà Trái Cây',
  'Hi-Tea Healthy',
  'Đá Xay - Choco',
  'Thưởng thức tại nhà',
  'Bánh - Snack',
] ;

//list coffee
List titleListCoffee =[
  'Cà Phê Sữa Đá',
  'Cà Phê Sữa Nóng',
  'Bạc Sỉu',
];

List listCoffeePrice=[
  '35.000đ',
  '35.000đ',
  '29.000đ',
];

List listImgCoffee=[
  'assets/orderImg/cf1.png',
  'assets/orderImg/cf2.png',
  'assets/orderImg/cf3.png',
];

List listImgCoffeedt = [
  'assets/orderImg/dtcf1.png',
  'assets/orderImg/dtcf2.png',
  'assets/orderImg/dtcf3.png',
];

//List fruit tea
List titleListFruitTea=[
  'Trà Long Nhãn',
  'Trà Đào Cam Sả - Ice',
  'Trà Đào Cam Sả - Hot',
];

List listFruitTeaPrice=[
  '51.000đ',
  '51.000đ',
  '51.000đ',
];

List listImgFruitTea=[
  'assets/orderImg/ft1.png',
  'assets/orderImg/ft2.png',
  'assets/orderImg/ft3.png',
];

List listImgFruitTeadt = [
  'assets/orderImg/dtft3.png',
  'assets/orderImg/dtft1.png',
  'assets/orderImg/dtft2.png',
];

//list Hi tea
List titleListHiTea=[
  'Hi-Tea Đào',
  'Hi-Tea Vải',
  'Hi-Tea Yuzu',
];

List listHiTeaPrice=[
  '51.000đ',
  '51.000đ',
  '51.000đ',
];

List listImgHiTea=[
  'assets/orderImg/ht1.png',
  'assets/orderImg/ht2.png',
  'assets/orderImg/ht3.png',
];

List listImgHiTeadt = [
  'assets/orderImg/dtht1.png',
  'assets/orderImg/dtht2.png',
  'assets/orderImg/dtht3.png',
];

//list IceDrink
List titleListIceDrink=[
  'Cà Phê Đá Xay - Lạnh',
  'Cookie Đá Xay',
  'Chocolate Đá Xay',
];

List listIceDrinkPrice=[
  '58.000đ',
  '58.000đ',
  '58.000đ',
];

List listImgIceDrink=[
  'assets/orderImg/id1.png',
  'assets/orderImg/id2.png',
  'assets/orderImg/id3.png',
];

List listImgIceDrinkdt = [
  'assets/orderImg/dtid1.png',
  'assets/orderImg/dtid2.png',
  'assets/orderImg/dtid3.png',
];

//list enjoy at home
List titleListEnjoyHome=[
  'Cà Phê Hòa Tan',
  'Cà Phê Sữa Hòa Tan',
  'Trà Lài LÁ Tearoma',
];

List listEnjoyHomePrice=[
  '39.000đ',
  '39.000đ',
  '69.000đ',
];

List listImgEnjoyHome=[
  'assets/orderImg/eh1.png',
  'assets/orderImg/eh2.png',
  'assets/orderImg/eh3.png',
];

List listImgEnjoyHomedt = [
  'assets/orderImg/dteh1.png',
  'assets/orderImg/dteh2.png',
  'assets/orderImg/dteh3.png',
];

//list Snack
List titleListSnack=[
  'Bánh Mì Que Pate',
  'Bánh Mì Que Pate Cay',
  'Bánh Mì VN Thịt Nguội',
];

List listSnackPrice=[
  '12.000đ',
  '12.000đ',
  '29.000đ',
];

List listImgSnack=[
  'assets/orderImg/sn1.png',
  'assets/orderImg/sn1.png',
  'assets/orderImg/sn2.png',
];

List listImgSnackdt = [
  'assets/orderImg/dts1.png',
  'assets/orderImg/dts1.png',
  'assets/orderImg/dts2.png',
];