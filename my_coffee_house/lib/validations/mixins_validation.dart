mixin CommonValidation{

  String? validatePhone(String? value){
    if (value!.length != 10) {
      return 'Mobile Number must be of 10 digit';
    }
    return null;
  }
}
