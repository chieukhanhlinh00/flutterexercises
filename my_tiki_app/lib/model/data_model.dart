//chat list
import 'package:flutter/material.dart';

//personal page
List imgList = [
  'assets/images/ava1.jpg',
  'assets/images/ava2.jpg',
  'assets/images/ava3.jpg',
  'assets/images/ava4.jpg',
  'assets/images/ava5.jpg',
  'assets/images/ava6.jpg',
];
List titleList = ['A', 'B', 'C', 'D', 'E', 'F'];
List subTitleList = [
  'Shop hoa',
  'Shop đồng phục',
  'Shop cos',
  'Shop quân trang',
  'Shop máy tính',
  'Shop ly'
];

//personal list
List perListTitle = [
  'Kết nối mạng xã hội',
  'Mã giảm giá',
  'Săn thưởng',
  'Đánh giá sản phẩm',
  'Quản lý đơn hàng',
  'Tiki tiếp nhận',
  'Đơn hàng chờ thanh toán',
  'Đơn hàng đang chờ vận chuyển',
  'Đơn hàng thành công',
  'Đơn hàng đã hủy',
  'Cài Đặt',
];

List perListIcons = [
  Icons.mediation_rounded,
  Icons.card_giftcard_rounded,
  Icons.volunteer_activism_outlined ,
  Icons.star_rate_rounded,
  Icons.task_outlined,
  Icons.add_task_outlined,
  Icons.payment_sharp,
  Icons.delivery_dining,
  Icons.check,
  Icons.disabled_by_default,
  Icons.settings,
];

//inbox page
class ChatMessage{
  String messageContent;
  String messageType;
  ChatMessage({required this.messageContent, required this.messageType});
}

List<ChatMessage> messages = [
  ChatMessage(messageContent: "Xin chào, tôi muốn mua một bó hoa cúc dại", messageType: "sender"),
  ChatMessage(messageContent: "Xin chào, bạn muốn hoa có màu gì?", messageType: "receiver"),
  ChatMessage(messageContent: "Một bó hoa cúc dại tím, cảm ơn.", messageType: "sender"),
  ChatMessage(messageContent: "Bạn có muốn mua kèm theo tấm thiệp không?", messageType: "receiver"),
  ChatMessage(messageContent: "Có chứ, viết lên thiệp giúp tôi là...", messageType: "sender"),
];

//surf screen
//discover page
List topListIconsDis =[
  'assets/images/tikilive.png',
  'assets/images/tikigiaitri.jpg',
  'assets/images/shoppingbag.jpg',
  'assets/images/save.png',
  'assets/images/Hashtag.png',
  'assets/images/credit.png',
  'assets/images/present.png',
];

List topListViewText = [
  'Tiki Live',
  'Giải trí',
  'Mua sắm',
  'Bảo hiểm'
  '#Tikixuhuong',
  'Mở thẻ - Nhận quà',
  'Nhận ngay Coupon',
];

