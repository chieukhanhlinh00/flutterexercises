import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'suggestion_page.dart';

class SideBar extends StatefulWidget {

  @override
  _SideBarState createState() => _SideBarState();
}

class _SideBarState extends State<SideBar> {
  int _selectedIndex = 0;
  // List destinations = [
  //
  // ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Row(
        children: [
          SingleChildScrollView(
            child: Container(
              width: 100,
              height: 1000,
              child: NavigationRail(
                backgroundColor: Colors.blue[200],
                indicatorColor: Colors.white,
                useIndicator: true,
                unselectedIconTheme: IconThemeData(color: Colors.white,),
                selectedIconTheme: IconThemeData(color: Colors.black),
                unselectedLabelTextStyle: TextStyle(color: Colors.black54),
                selectedLabelTextStyle: TextStyle(color: Colors.black),
                selectedIndex: _selectedIndex,
                onDestinationSelected: (int index){
                  setState(() {
                    _selectedIndex = index;
                  });
                },
                labelType: NavigationRailLabelType.selected,
                destinations: [
                  NavigationRailDestination(
                    icon: Icon(Icons.stars_outlined ),
                    label: Text('Gợi ý cho bạn', textAlign: TextAlign.center,),
                  ),
                  NavigationRailDestination(
                    icon: Icon(Icons.toys_outlined  ),
                    label: Text('Đồ Chơi - Mẹ & Bé',textAlign: TextAlign.center),
                  ),
                  NavigationRailDestination(
                    icon: Icon(Icons.emoji_food_beverage_outlined ),
                    label: Text('NGON',textAlign: TextAlign.center),
                  ),
                  NavigationRailDestination(
                    icon: Icon(Icons.phonelink ),
                    label: Text('Điện thoại - Máy tính bản',textAlign: TextAlign.center),
                  ),
                  NavigationRailDestination(
                    icon: Icon(Icons.auto_awesome_outlined ),
                    label: Text('Làm đẹp, sức khỏe',textAlign: TextAlign.center),
                  ),
                  NavigationRailDestination(
                    icon: Icon(Icons.lightbulb_outline_rounded  ),
                    label: Text('Điện gia dụng',textAlign: TextAlign.center),
                  ),
                  NavigationRailDestination(
                    icon: Icon(Icons.directions_car_outlined ),
                    label: Text('Ô tô - Xe máy - Xe đạp',textAlign: TextAlign.center),
                  ),
                  NavigationRailDestination(
                    icon: Icon(Icons.book_outlined ),
                    label: Text('Nhà sách Tiki',textAlign: TextAlign.center),
                  ),
                  NavigationRailDestination(
                    icon: Icon(Icons.sports_baseball_outlined ),
                    label: Text('Thể thao dã ngoại',textAlign: TextAlign.center),
                  ),
                  NavigationRailDestination(
                    icon: Icon(Icons.camera_outlined ),
                    label: Text('Máy ảnh - Quay phim',textAlign: TextAlign.center),
                  ),
                  NavigationRailDestination(
                    icon: Icon(Icons.headphones_outlined  ),
                    label: Text('Thiết bị số - Phụ kiện số',textAlign: TextAlign.center),
                  ),
                  NavigationRailDestination(
                    icon: Icon(Icons.card_giftcard_outlined ),
                    label: Text('Voucher - Dịch vụ',textAlign: TextAlign.center),
                  ),
                ],
              ),
            ),
          ),
          const VerticalDivider(thickness: 1, width: 1),
          Expanded(child: Center(
            child: Text('selectedIndex: $_selectedIndex'),
          ),)
        ],
      ),
    );
  }

}