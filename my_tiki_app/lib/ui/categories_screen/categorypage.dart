import 'package:flutter/material.dart';
import '../cart_screen/cartpage.dart';
import '../search_screen/search_page.dart';
import 'cate_body/sidebar.dart';

class CategoryPage extends StatefulWidget{
  static const route = '/category';
  @override
  State<CategoryPage> createState() => _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage>{
  int currentIndex = 0;

  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.lime,
        title: Container(
          width: double.infinity,
          height: 40,
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.circular(5)),
          child: Center(
            child: TextField(
              decoration: InputDecoration(
                hintText: 'Sản phẩm, thương hiệu, và mọi thứ cần thiết trong cuộc sống',
                prefixIcon: Icon(Icons.search),),
              onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (_)
              => SearchPage())),
            ),
          
          ),

        ),
        actions: [
          IconButton(
            icon: Icon(Icons.shopping_cart_outlined),
            onPressed: (){
              Navigator.of(context).pushNamed(CartPage.route);
            },
          )
        ],),

      body: SideBar(),
    );
  }
}




