import 'package:flutter/material.dart';
import '../surf_screen/tabbars/community_screen/community_screen.dart';
import '../surf_screen/tabbars/follow_screen/follow_screen.dart';
import '../surf_screen/tabbars/discover_screen/discover_screen.dart';

class SurfPage extends StatefulWidget{
  static const route = '/surf';
  State<SurfPage> createState() => _SurfPageState();
}

class _SurfPageState extends State<SurfPage>{
  int currentIndex = 0;

  Widget build(BuildContext context){
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          bottom: TabBar(
            unselectedLabelColor: Colors.white,
            indicator: BoxDecoration(
              color: Colors.white54,
                borderRadius: BorderRadius.circular(50),
            ),
            labelStyle: TextStyle(fontWeight: FontWeight.w800,),
            tabs: [
              Tab(child: Text('Khám phá'), ),
              Tab(child: Text('Đang theo'), ),
              Tab(child: Text('Cộng đồng'), ),
            ],
          ),
          centerTitle: true,
          elevation: 0,
          title: Text('Lướt tin'),
        ),
        body: TabBarView(
          physics: NeverScrollableScrollPhysics(),
          children: [
            DiscoverScreen(),
            FollowScreen(),
            CommunityScreen(),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {},
          child: const Icon(Icons.edit_outlined),
        ),

      ),

    );
  }
}