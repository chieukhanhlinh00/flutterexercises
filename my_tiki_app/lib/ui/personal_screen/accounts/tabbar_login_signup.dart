import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'loginpage.dart';
import 'signuppage.dart';

class LoginSignup extends StatefulWidget{
  static const route = '/tablogsign';
  @override
  _LoginSignupState createState() => _LoginSignupState();
}

class _LoginSignupState extends State<LoginSignup>{
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text('Đăng nhập/ Đăng ký'),
        centerTitle: true,
        bottom: TabBar(
          unselectedLabelColor: Colors.white,
          indicator: BoxDecoration(
            color: Colors.white54,
          ),
          labelStyle: TextStyle(fontWeight: FontWeight.w800,),
          tabs: [
            Tab(child: Text('Đăng nhập'), ),
            Tab(child: Text('Đăng ký'), ),
          ],
        ),
      ),
        body: TabBarView(
          physics: NeverScrollableScrollPhysics(),
          children: [
            LoginPage(),
            SignupPage(),
          ],
        ),
      ),
    );
  }
}