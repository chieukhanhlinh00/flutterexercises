import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_tiki_app/ui/home_screen/homepage.dart';
import 'package:my_tiki_app/ui/personal_screen/accountpage.dart';
import '../../../validation/mixins_validation.dart';

class LoginPage extends StatefulWidget{
  static const route = '/login';
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> with CommonValidation {
  final formKey = GlobalKey<FormState>();
  late String emailAddress;
  late String password;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      child: Form(
          key: formKey,
          child: Column(
            children: <Widget>[
              fieldEmailAddress(),
              Container(
                margin: EdgeInsets.only(top: 15),
              ),
              fieldPassword(),
              Container(
                margin: EdgeInsets.only(top: 15),
              ),
              loginBtn(),
              middleText(),
              aLoginButton(),
            ],
          )),
    );
  }

  Widget fieldEmailAddress() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: "Email address",
      ),
      validator: validateEmail,
      onSaved: (value){
        emailAddress = value as String;
      },
    );
  }

  Widget fieldPassword() {
    return TextFormField(
      obscureText: true,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: "Password",
      ),
      validator: validatePassword,
      onSaved: (value){
        password = value as String;
      },
    );
  }

  Widget loginBtn() {
    return Container(
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            minimumSize: Size(400, 50),
            primary: Colors.red,
            onPrimary: Colors.white,
          ),
        onPressed: () {

          if(formKey.currentState!.validate()){
            //Call API Auth from Backend
            formKey.currentState!.save();
            print('$emailAddress,$password, Demo only:$emailAddress, $password');
            Navigator.of(context).pushNamed(HomePage.route);
          }
          // print('Clicked login button');

        },
        child: Text('ĐĂNG NHẬP')
        ),
        );
  }

  Widget middleText() {
    return Container(
      child: DefaultTextStyle(
        style: TextStyle(color: Colors.black54,),
        child: Column(
          children: [
            Padding(padding: EdgeInsets.all(8),),
            Text('Khi đăng nhập, bạn đã chấp nhận điều khoản sử dụng',textAlign: TextAlign.center,),
            Text('Quên mật khẩu?',style: TextStyle(color: Colors.blueAccent),),

            Padding(padding: EdgeInsets.only(top: 150)),
            Text('Hoặc đăng nhập với',
              style: TextStyle(color: Colors.black54),
              textAlign: TextAlign.center,
            )
          ],
        ),
        
      ),
    );
  }

  Widget aLoginButton() {
    return Container(
      padding: EdgeInsets.only(top: 20),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              GestureDetector(
              child: CircleAvatar(
                backgroundImage: AssetImage('assets/logo/facebook.png'),
                maxRadius: 20,
                backgroundColor: Colors.grey[100],
              ),
                onTap: () => {},
              ),
              GestureDetector(
              child: CircleAvatar(
                backgroundImage: AssetImage('assets/logo/google.png'),
                maxRadius: 20,
                backgroundColor: Colors.grey[100],
                foregroundColor: Colors.black,
              ),
                onTap: () => {print('clicked')},
              ),
              GestureDetector(
              child: CircleAvatar(
                backgroundImage: AssetImage('assets/logo/zalo.jpg'),
                maxRadius: 20,
                backgroundColor: Colors.grey[100],
              ),
                onTap: () => {},
              ),
            ],
              ),
        ],
      ),

    );
  }

}