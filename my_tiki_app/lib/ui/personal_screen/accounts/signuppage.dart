import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../home_screen/homepage.dart';
import '../accountpage.dart';

class SignupPage extends StatefulWidget{
  static const route = '/signup';
  @override
  _SignupPageState createState() => _SignupPageState();
}

class _SignupPageState extends State<SignupPage>{
  @override
  Widget build(BuildContext context) {
    final formKey = GlobalKey<FormState>();
    return Container(
      padding: EdgeInsets.all(20),
      child: Form(
      key: formKey,
        child: Column(
          children: <Widget>[
            fieldName(),
            Container(
              margin: EdgeInsets.only(top: 15),
            ),
            fieldPhoneNum(),
            Container(
              margin: EdgeInsets.only(top: 15),
            ),
            fieldEmail(),
            Container(
              margin: EdgeInsets.only(top: 15),
            ),
            fieldPass(),
            Container(
              margin: EdgeInsets.only(top: 15),
            ),
            fieldBirth(),
            Container(
              margin: EdgeInsets.only(top: 15),
            ),
            Gender(),
            Container(
              margin: EdgeInsets.only(top: 15),
            ),
            signupBtn(),
            lastText(),
          ],
        ),
      ),
    );
  }
  Widget fieldName() {
    return TextFormField(
      keyboardType: TextInputType.name,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: "Họ Tên",
      ),
    );
  }

  Widget fieldPhoneNum() {
    return TextFormField(
      keyboardType: TextInputType.phone,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: "Số điện thoại",
      ),
    );
  }

  Widget fieldEmail() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: "Email",
      ),
    );
  }

  Widget fieldPass() {
    return TextFormField(
      keyboardType: TextInputType.visiblePassword,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: "Mật khẩu",
      ),
    );
  }

  Widget fieldBirth() {
    return TextFormField(
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: "Ngày sinh",
      ),
    );
  }

  Widget Gender(){
    String _selectedGender = 'male';
    return Column(
      children:[
        // Padding(
        //   padding: EdgeInsets.symmetric(vertical: 9),
        // ),
        Row(
          children: [
            Radio<String>(
              value: 'male',
              groupValue: _selectedGender,
              onChanged: (value) {
                setState(() {
                  _selectedGender = 'male';
                });
                      },
                    ),
        Text('Nam'),
        Radio<String>(
          value: 'female',
          groupValue: _selectedGender,
          onChanged: (value) {
            setState(() {
              _selectedGender = 'female';
            });
            },
        ),
        Text('Nữ'),
          ]
            ),
          ],
    );
  }

  Widget signupBtn(){
    return Container(
      child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            minimumSize: Size(400, 50),
            primary: Colors.red,
            onPrimary: Colors.white,
          ),
          onPressed: () {
              Navigator.of(context).pushNamed(HomePage.route);
          },
          child: Text('ĐĂNG KÝ')
      ),
    );
  }

  Widget lastText(){
    return Container(
      padding: EdgeInsets.only(top: 10),
      child: Text('Khi đăng ký, ban đã chấp nhận điều khoản sử dụng',
      style: TextStyle(fontSize: 13, color: Colors.black54),),
    );
  }

}