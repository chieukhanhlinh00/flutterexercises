import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../model/data_model.dart';
import '../cart_screen/cartpage.dart';
import 'accounts/tabbar_login_signup.dart';

class AccountPage extends StatefulWidget {
  static const route = '/personal';
  State<AccountPage> createState() => _AccountPageState();
}

class _AccountPageState extends State<AccountPage> {
  int currentIndex = 0;

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.blueAccent,
        centerTitle: true,
        title: Text('Cá Nhân'),
        actions: [
          IconButton(
            padding: EdgeInsets.only(right: 12),
            icon: Icon(Icons.shopping_cart_outlined),
            onPressed: () {
              Navigator.of(context).pushNamed(CartPage.route);
            },
          ),
        ],
      ),
      body:  SingleChildScrollView(
        child:Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            UserInform(),
            _ListOrderHis(),
          ],
        ), ),
    );
  }
}

class UserInform extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 80,
      width: double.infinity,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [Colors.blueAccent,Colors.blue ]
        ),
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(20),
          bottomRight: Radius.circular(20),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Chào mừng bạn đến với Tiki',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 8,
            ),
            TextButton(
              child: Text('Đăng nhập/ Đăng ký'),
              style: TextButton.styleFrom(
                  primary: Colors.blue, backgroundColor: Colors.white),
              onPressed: () {
                Navigator.of(context).pushNamed(LoginSignup.route);
              },
            ),
          ],
        ),
      ),
    );
  }
}

class _ListOrderHis extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey[200],
      child: ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: perListTitle.length,
        physics: NeverScrollableScrollPhysics(),
      itemBuilder: (BuildContext context, int index) =>
          GestureDetector(
            child: Card(
              elevation: 0,
              margin: EdgeInsets.only( top: 10),
              child: ListTile(
                tileColor: Colors.white,
                title: Text(perListTitle[index]),
                leading: Icon(perListIcons[index]),
                trailing: Icon(Icons.keyboard_arrow_right_rounded),
          ),
        ),
      ),
      ),
        //
        );
  }
}
