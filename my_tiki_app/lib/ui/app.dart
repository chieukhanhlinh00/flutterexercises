import 'package:flutter/material.dart';
import 'package:my_tiki_app/ui/chat_screen/inbox_page.dart';
import 'package:my_tiki_app/ui/home_screen/homepage.dart';
import 'package:my_tiki_app/ui/personal_screen/accounts/loginpage.dart';
import 'package:my_tiki_app/ui/personal_screen/accounts/signuppage.dart';
import 'package:my_tiki_app/ui/personal_screen/accounts/tabbar_login_signup.dart';
import '../ui/categories_screen/categorypage.dart';
import '../ui/personal_screen/accountpage.dart';
import '../ui/surf_screen/surfpage.dart';
import '../ui/chat_screen/chatpage.dart';
import '../ui/cart_screen/cartpage.dart';
import '../ui/search_screen/search_page.dart';

class App extends StatelessWidget{
  Widget build(BuildContext context){
    return MaterialApp(
      title: "TiKi",
      theme: ThemeData(
        primaryColor: Colors.lime,),
      routes: {
        '/': (context) => MyHomePage(),
        '/home': (context) => HomePage(),
        '/cart': (context) => CartPage(),
        '/chat': (context) => ChatPage(),
        '/category': (context) => CategoryPage(),
        '/personal': (context) => AccountPage(),
        '/surf': (context) => SurfPage(),
        '/search': (context) => SearchPage(),
        '/tablogsign': (context) => LoginSignup(),
        '/login': (context) => LoginPage(),
        '/signup': (context) => SignupPage(),
        '/inbox': (context) => InboxPage(),

      },
      initialRoute: '/',
    );
  }
}

class MyHomePage extends StatefulWidget{
  const MyHomePage({Key? key}) : super(key: key);
  static const route = '/';
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage>{
  int currentIndex = 0;
  List screens = [HomePage(), CategoryPage(),SurfPage(), ChatPage(), AccountPage() ];

  void updateIndex( int value){
    setState(() {
      currentIndex = value;
    });
  }
@override
  Widget build(BuildContext context){
    return Scaffold(

      body: screens[currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: currentIndex,
        onTap: updateIndex,
        selectedItemColor: Colors.blue[700],
        selectedFontSize: 13,
        unselectedFontSize: 13,
        iconSize: 30,
        backgroundColor: Colors.white,
        items: [
          BottomNavigationBarItem(
            label: "Trang chủ",
            icon: Icon(Icons.home_filled),
          ),

          BottomNavigationBarItem(
            label: "Danh mục",
            icon: Icon(Icons.grid_view_rounded),
          ),

          BottomNavigationBarItem(
            label: "Lướt",
            icon: Icon(Icons.local_fire_department_sharp),
          ),

          BottomNavigationBarItem(
            label: "Chat",
            icon: Icon(Icons.chat_bubble_outline),
          ),

          BottomNavigationBarItem(
            label: "Cá nhân",
            icon: Icon(Icons.person_sharp ),
          ),
        ],

      ),
    );

  }

}