import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../cart_screen/cartpage.dart';
import '../search_screen/search_page.dart';

class HomePage extends StatefulWidget{
  static const route = '/';
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>{
  int currentIndex = 0;

  Widget build(BuildContext context){
    return DefaultTabController(length: 10,
        child:Scaffold(

          body: CustomScrollView(
              slivers: [
                SliverAppBar(

                  floating: true,
                  pinned: true,
                  snap: false,
                  centerTitle: true,
                  title: Text('TiKi CLone'),
                  backgroundColor: Colors.lime,


                  bottom: AppBar(
                    elevation: 0.0,
                    backgroundColor: Colors.lime,
                    title: Container(
                      width: double.infinity,
                      height: 40,
                      decoration: BoxDecoration(
                          color: Colors.white, borderRadius: BorderRadius.circular(5)),
                      child: Center(
                        child: TextField(
                          decoration: InputDecoration(
                            hintText: 'Bạn tìm gì hôm nay?',
                            prefixIcon: Icon(Icons.search),
                          ),
                          onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (_)
                          => SearchPage())),
                        ),
                      ),

                    ),
                    actions: [
                      IconButton(
                        padding: EdgeInsets.only(right: 12),
                        icon: Icon(Icons.notifications_none_rounded  ),
                        onPressed: (){
                          //  do this later
                        },
                      ),
                      IconButton(
                        icon: Icon(Icons.shopping_cart_outlined),
                        onPressed: (){
                          Navigator.of(context).pushNamed(CartPage.route);
                        },
                      ),
                    ],
                  ),

                ),


                SliverList(
                  delegate: SliverChildListDelegate([
                    Container(
                      color: Colors.lime,
                      child: DefaultTabController(
                        length: 10,
                        child: TabBar(
                          indicatorColor: Colors.lime,
                          isScrollable: true,
                            tabs: [
                              Tab(text: 'Thịt, Rau Củ',),
                              Tab(text: 'Bách Hóa',),
                              Tab(text: 'Nhà Cửa',),
                              Tab(text: 'Điện Tử',),
                              Tab(text: 'Thiết Bị Số',),
                              Tab(text: 'Điện Thoại',),
                              Tab(text: 'Mẹ & Bé',),
                              Tab(text: 'Làm Đẹp',),
                              Tab(text: 'Gia Dụng',),
                              Tab(text: 'Thời Trang',),
                            ],
                        ),

                      ),
                    ),
                    Container(
                      height: 400,
                      color: Colors.lime,
                      child: Center(
                      child: Text('Trang 1',),
                      ),
                    ),

                    // Container(
                    //   child: DefaultTabController(
                    //     length: 10,
                    //     child: TabBar(
                    //       isScrollable: true,
                    //       tabs: [
                    //         Tab(text: 'Thịt, Rau Củ',),
                    //         Tab(text: 'Bách Hóa',),
                    //         Tab(text: 'Nhà Cửa',),
                    //         Tab(text: 'Điện Tử',),
                    //         Tab(text: 'Thiết Bị Số',),
                    //         Tab(text: 'Điện Thoại',),
                    //         Tab(text: 'Mẹ & Bé',),
                    //         Tab(text: 'Làm Đẹp',),
                    //         Tab(text: 'Gia Dụng',),
                    //         Tab(text: 'Thời Trang',),
                    //       ],
                    //     ),

                    //   ),
                    // ),
                    Container(
                      height: 1000,
                    ),
                  ]),
                ),
              ],
          ),
    ),
    );
  }
}



