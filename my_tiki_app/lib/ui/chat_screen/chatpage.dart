import 'package:flutter/material.dart';
import 'package:my_tiki_app/ui/chat_screen/inbox_page.dart';
import '../../model/data_model.dart';
import '../cart_screen/cartpage.dart';

class ChatPage extends StatefulWidget{
  static const route = '/chat';
  State<ChatPage> createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage>{
  int currentIndex = 0;
  Widget build(BuildContext context){
    return Scaffold(
      backgroundColor: Colors.white54,
      appBar: AppBar(
        elevation: 0,
        centerTitle: true,
        backgroundColor: Colors.black45,
        title: Text('Chat'),
        actions: [
          IconButton(
            padding: EdgeInsets.only(right: 12),
            icon: Icon(Icons.shopping_cart_outlined),
            onPressed: (){
              Navigator.of(context).pushNamed(CartPage.route);
            },
          ),
        ],
      bottom: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.white,
        title: Container(
          width: double.infinity,
          height: 40,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(5),
              border: Border.all(
                color: Colors.black45
              ),
          ),

          child: Center(
            child: TextField(
              decoration: InputDecoration(
                hintText: 'Tìm theo người dùng...',
                prefixIcon: Icon(Icons.search),
              ),
            ),
          ),
        ),
      ),
      ),

      body: ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: titleList.length,
        itemBuilder: (context, index) => Container(
          decoration: BoxDecoration(
              border: Border.symmetric(
                  horizontal: BorderSide(color: Colors.black, width: 0.2)
              ),
          ),
          child: Card(
            margin: EdgeInsets.only(bottom: 1),
            elevation: 0,
            child: ListTile(
              onTap: () {
                Navigator.of(context).pushNamed(InboxPage.route);
                },
              title: Text(titleList[index]),
              subtitle: Text(subTitleList[index]),
              leading: GestureDetector(
                child: CircleAvatar(
                backgroundImage: AssetImage(imgList[index]),
                maxRadius: 20,
                backgroundColor: Colors.grey[100],
                ),
              ),
              trailing: Icon(Icons.keyboard_arrow_right_rounded),
              ),

              ),
            ),
          ),
    );
  }
}