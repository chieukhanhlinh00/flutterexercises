import 'package:flutter/material.dart';

import '../../model/data_model.dart';

class InboxPage extends StatefulWidget{
  static const route = '/inbox';
  @override
  _InboxPageState createState() => _InboxPageState();
}

class _InboxPageState extends State<InboxPage>{
  @override
  Widget build(BuildContext context) {
    // String title = ModalRoute.of(context)!.settings.arguments as String;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        flexibleSpace: SafeArea(
        child: Container(
        padding: EdgeInsets.only(right: 16),
          child: Row(
            children: <Widget>[
           IconButton(
             onPressed: (){
              Navigator.pop(context);
              },
            icon: Icon(Icons.arrow_back,color: Colors.black,),
            ),
              SizedBox(width: 2,),
              CircleAvatar(
                backgroundImage: AssetImage('assets/images/ava1.jpg'),
                  maxRadius: 20,
                  ),
              SizedBox(width: 12,),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("A",style: TextStyle( fontSize: 16 ,fontWeight: FontWeight.w600),),
                    SizedBox(height: 6,),
                    Text("Online",style: TextStyle(color: Colors.grey.shade600, fontSize: 13),),
                  ],
                ),
              ),
              IconButton(
                  onPressed: () => {},
                  icon: Icon(Icons.more_horiz_outlined,color: Colors.black54,),
              )
            ],
          ),
        ),
        ),

        ),
      body: Container(
        child: bottomBox(),
      ),

    );
  }
}

class bottomBox extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        ListView.builder(
          itemCount: messages.length,
          shrinkWrap: true,
          padding: EdgeInsets.only(top: 10,bottom: 10),
          physics: NeverScrollableScrollPhysics(),
          itemBuilder: (context, index){
            return Container(
              padding: EdgeInsets.only(left: 15,right: 15,top: 10,bottom: 10),
              child:Align(
                  alignment: (messages[index].messageType == "receiver"?Alignment.topLeft:Alignment.topRight),
                  child: Container(
                  decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: (messages[index].messageType  == "receiver"?Colors.grey.shade200:Colors.blue[200]),
                  ),
                  padding: EdgeInsets.all(16),
                    child: Text(messages[index].messageContent, style: TextStyle(fontSize: 15),),
            ),
              ),
            );
          },
        ),


        Align(
          alignment: Alignment.bottomLeft,
          child: Container(
            padding: EdgeInsets.only(left: 10,bottom: 10,top: 10),
            height: 60,
            width: double.infinity,
            color: Colors.white,
            child: Row(
              children: <Widget>[
                GestureDetector(
                  onTap: (){
                  },
                child: Icon(Icons.image_outlined, color: Colors.blue, size: 30, ),
                  ),
                SizedBox(width: 15,),
                Expanded(
                  child: TextField(
                    decoration: InputDecoration(
                        hintText: "Nhập nội dung chat...",
                        hintStyle: TextStyle(color: Colors.black54),
                        border: InputBorder.none
                    ),
                  ),
                ),
                SizedBox(width: 15,),
                FloatingActionButton(
                  onPressed: (){},
                  child: Icon(Icons.send,color: Colors.white,size: 18,),
                  backgroundColor: Colors.blue,
                  elevation: 0,
                ),
              ],

            ),
          ),
        ),
      ],
    );
  }

}