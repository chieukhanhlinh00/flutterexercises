import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../home_screen/homepage.dart';

class CartPage extends StatefulWidget{
  static const route = '/cart';
  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        centerTitle: true,
        title: Text('Giỏ Hàng'),
      ),
      body: Container(
        alignment: Alignment.center,
        child: Padding(
        padding:  const EdgeInsets.all(10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Bạn chưa có sản phẩm nào trong giỏ hàng',
                  style: TextStyle(
                      color: Colors.black54,
                      fontSize: 18,
                  )),
              SizedBox(
                height: 8,
              ),
              TextButton(
                child: Text('tiếp tục mua sắm'.toUpperCase()),
                style: TextButton.styleFrom(
                  primary: Colors.white,
                  backgroundColor: Colors.red,
                  minimumSize: Size(400, 50),
                  ),
                onPressed: () {
                  Navigator.of(context).pushNamed(HomePage.route);
                },)
            ],
          ),
        ),
      ),
    );
  }
  
}