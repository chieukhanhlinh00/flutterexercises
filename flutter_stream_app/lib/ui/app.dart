import 'package:flutter/material.dart';
import '../stream/word.dart';

class App extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<StatefulWidget> {
  WordStream wordStream = WordStream();
  String englishWord = 'Hello, world';
  int number = 0;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stream Words',

      home: Scaffold(
        backgroundColor: Colors.teal,
        appBar: AppBar(title: Text('App Bar'),),
        body: Center(
          child: Column(
            children: [
              Text("English Words", style: TextStyle(fontSize: 20.0), textAlign: TextAlign.center),
              Text(englishWord,  style: TextStyle(fontSize: 40.0, fontWeight: FontWeight.bold), textAlign: TextAlign.center),
              Text("Number: $number",style: TextStyle(fontSize: 20.0), textAlign: TextAlign.center),
            ],
          ),

        ),
        floatingActionButton: FloatingActionButton(
          child: Text('Start'),
          onPressed: () {
            changeWord();
            count();
          },
        ),
      ),
    );
  }

  changeWord() async {
    wordStream.getWord().listen((eventWord) {
      setState(() {
        print(eventWord);
        englishWord = eventWord!;
      });
    });
  }

  count() async {
    wordStream.countWord().listen((eventCount) {
      setState(() {
        print("Number : $eventCount");
        number = eventCount!;
      });
    });
  }
}
