import 'package:flutter/material.dart';
import 'package:image_stream/stream/imagestream.dart';
import 'package:image_stream/model/image.dart';

class App extends StatefulWidget {


  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }

}

class HomePageState extends State<StatefulWidget> {
  GetImageStream  image = GetImageStream ();
  List<ImageModel> imageList = [];

  HomePageState() {
    getStreamContent();
  }

  getStreamContent() async {
    image.getImage().listen((imageObj) => setState(() {
      imageList.add(imageObj!);
    }));
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stream Color',

      home: Scaffold(
        appBar: AppBar(title: Text('App Bar'),),
        backgroundColor: Colors.lightGreen,
        body:ListView.builder(
            itemCount: imageList.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                padding: const EdgeInsets.all(10),
                // Prevent null value by using replace value
                child: Image.network(imageList[index].url ?? ""),
              );
            })

      ),
    );
  }
}
