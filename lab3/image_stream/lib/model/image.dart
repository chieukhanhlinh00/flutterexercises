class ImageModel {
  int? id;
  String? url;

  ImageModel(this.id, this.url);

  ImageModel.fromJson(jsonObject) {
    id = jsonObject['id'];
    url = jsonObject['url'];
  }

  @override
  String toString() {
    return '($id, $url)';
  }
}
