import 'package:flutter/material.dart';
import '../stream/textstream.dart';

class App extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<StatefulWidget> {
  TextStream textStream = TextStream();
  List<String> textList = [];

  HomePageState(){
    getStreamContent();
  }

  getStreamContent() async {
    textStream.getText().listen((text) => setState(() {
      textList.add(text);
    }));
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Messages',

      home: Scaffold(
        backgroundColor: Colors.teal,
        appBar: AppBar(title: Text('App Bar'),),
        body:
              ListView.builder(
                  itemCount: textList.length,
                  itemBuilder: (context, index){
                return Container(
                  padding: const EdgeInsets.all(5),
                    child: Text(textList[index], style: TextStyle(fontSize: 40.0, fontWeight: FontWeight.bold), textAlign: TextAlign.center));

             })
          ),
      );
  }


}
