import 'dart:convert';
import 'package:flutter/material.dart';
import 'dart:math';

class TextStream {

  Stream<String> getText() async*{
    final String streamText = "Hello, world";

    yield* Stream.periodic(Duration(seconds: 5), (int t) {
      return streamText;
    });
  }
}
