import 'package:flutter/material.dart';
import 'package:area/area.dart';
void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Demo',
      home: PackageScreen(),
    );
  }
}

class PackageScreen extends StatefulWidget {
  const PackageScreen({Key? key}) : super(key: key);

  @override
  _PackageScreenState createState() => _PackageScreenState();
}

class _PackageScreenState extends State<PackageScreen> {
  final TextEditingController yourText = TextEditingController();
  String result = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Package App'),
      ),
      body: Column(
        children: [
          AppTextField(yourText, 'Nhập chuỗi'),
          Padding(
            padding: EdgeInsets.all(24),
          ),
          ElevatedButton(
            child: Text('Result'),
            onPressed: () {
              String text = yourText.text;
              String res = countText(text);
              setState(() {
                result = res;
              });
            },
          ),
          Padding(
            padding: EdgeInsets.all(24),
          ),
          Text(result),
        ],
      ),
    );
  }
}

class AppTextField extends StatelessWidget {
  final TextEditingController controller;
  final String label;

  AppTextField(this.controller, this.label);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(24),
      child: TextField(
        controller: controller,
        decoration: InputDecoration(hintText: label),
      ),
    );
  }
}
