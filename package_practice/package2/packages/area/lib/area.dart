library area;
import 'package:intl/intl.dart';

String sum(int firstNum, int secondNum) {
  int result = firstNum + secondNum;
  final formatter = NumberFormat('#');
  return formatter.format(result);
}


