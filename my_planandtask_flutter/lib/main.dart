import 'package:flutter/material.dart';
import 'package:my_planandtask_flutter/plan_provider.dart';
import 'package:my_planandtask_flutter/views/plan_creator_screen.dart';
import './views/plan_screen.dart';


void main() => runApp(MasterPlanApp());

class MasterPlanApp extends StatelessWidget{
  Widget build(BuildContext context){
    return MaterialApp(
      theme: ThemeData(primarySwatch:  Colors.purple),
      home: const PlanCreatorScreen(),
    );
  }
}