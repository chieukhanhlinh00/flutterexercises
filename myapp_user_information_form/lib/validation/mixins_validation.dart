mixin CommonValidation{
  String? validateEmail(String? value){
    if (!value!.contains('@')) {
      return 'Please input valid email.';
    }
    return null;
  }

  String? validateLastname(String? value){
    if (!value!.contains(r'^[a-zA-Z]')) {
      return 'last name must not have rare symbol';
    }
    return null;
  }

  String? validateFirstname(String? value){
    if (!value!.contains(r'^[a-zA-Z]')) {
      return 'first name must not have rare symbol';
    }
    return null;
  }

  String? validateBirthYear(int? value){
    if (value!.bitLength < 4) {
      return 'Year of birth must have 4 numbers';
    }
    return null;
  }

  String? validateAddress(String? value){
    if (value!.isEmpty) {
      return 'Year of birth must have 4 numbers';
    }
    return null;
  }
}