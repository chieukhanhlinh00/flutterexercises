import 'package:flutter/material.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Input your information',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Information Bar'),
        ),
        body: InfoScreen(),
      ),
    );
  }
}

class InfoScreen extends StatefulWidget {
  @override
  _InfoScreenState createState() => _InfoScreenState();
}

class _InfoScreenState extends State<InfoScreen>{
  final formKey = GlobalKey<FormState>();
  late String emailAddress;
  late String lastName;
  late String firstName;
  late int birthYear;
  late String address;

  Widget build(BuildContext context){
    return Container(
      child: Form(
        key: formKey,
        child: Column(
          children: <Widget>[
            fieldEmailAddress(),
            Container(
              margin: EdgeInsets.only(top: 20),
            ),
            fieldLastName(),
            Container(
              margin: EdgeInsets.only(top: 20),
            ),
            fieldFirstName(),
            Container(
              margin: EdgeInsets.only(top: 20),
            ),
            fieldBirthYear(),
            Container(
              margin: EdgeInsets.only(top: 20),
            ),
            fieldAddress(),
            Container(
              margin: EdgeInsets.only(top: 20),
            ),
            confirmBtn(),
          ],
        )),
    );
  }

  Widget fieldEmailAddress() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        icon: Icon(Icons.person),
        border: OutlineInputBorder(),
        labelText: "Email address",
      ),
      validator: (value){
        if(!value!.contains('@')) {
          return 'Please input valid email.';
        }
        return null;
      },

      onSaved: (value){
        emailAddress = value as String;
      },
    );
  }

  Widget fieldLastName() {
    return TextFormField(
      keyboardType: TextInputType.name,
      decoration: InputDecoration(
        icon: Icon(Icons.person),
        border: OutlineInputBorder(),
        labelText: "Your last name",
      ),
      validator: (value){
        if(value == null || value.isEmpty) {
          return 'Invalid name.';
        }
        return null;
      },

      onSaved: (value){
        lastName = value as String;
      },
    );
  }

  Widget fieldFirstName() {
    return TextFormField(
      keyboardType: TextInputType.name,
      decoration: InputDecoration(
        icon: Icon(Icons.person),
        border: OutlineInputBorder(),
        labelText: "Your first name",
      ),
      validator: (value){
        if(value == null || value.isEmpty) {
          return 'Invalid name.';
        }
        return null;
      },

      onSaved: (value){
        firstName = value as String;
      },
    );
  }

  Widget fieldBirthYear() {
    return TextFormField(
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        icon: Icon(Icons.calendar_month),
        border: OutlineInputBorder(),
        labelText: "Your year of birth",
      ),
      validator: (value){
        if(value!.isEmpty) {
          return 'Invalid year.';
        }
        return null;
      },

      onSaved: (value){
        birthYear = value as int;
      },
    );
  }

  Widget fieldAddress() {
    return TextFormField(
      keyboardType: TextInputType.streetAddress,
      decoration: InputDecoration(
        icon: Icon(Icons.home),
        border: OutlineInputBorder(),
        labelText: "Your home address",
      ),
      validator: (value){
        if(value!.isEmpty) {
          return 'Invalid address.';
        }
        return null;
      },

      onSaved: (value){
        address = value as String;
      },
    );
  }

  Widget confirmBtn(){
    return ElevatedButton(
        onPressed: () {

          if(formKey.currentState!.validate()){
            //Call API Auth from Backend
            formKey.currentState!.save();
            print('$emailAddress, $lastName, $firstName, $birthYear, $address');
          }
          print('Clicked confirm button');
        },
        child: Text('Confirm'));
  }


}

