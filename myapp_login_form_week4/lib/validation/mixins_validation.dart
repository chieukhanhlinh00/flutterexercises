mixin CommonValidation{
  String? validateEmail(String? value){
    if (!value!.contains(RegExp(r'^[^@]+@[^@]+\.[^@]+'))) {
      return 'Please input valid email.';
    }
    return null;
  }

  String? validatePassword(String? value){
    if (value!.length < 8) {
      return 'Password must be at least 6 chars';
    }
    else if(!value.contains(RegExp(r'(?=.*[a-z])'))){
      return 'Password must have at least one lower case';
    }
    else if(!value.contains(RegExp(r'(?=.*[A-Z])')) || !value.contains(RegExp(r'(?=.*[!@#$%^&*,. ?":{}|<>])' )) ){
      return 'Password must have at least one upper case or one special character';
    }
    return null;
  }
}