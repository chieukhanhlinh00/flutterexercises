import 'package:flutter/material.dart';
import 'package:flutter_final_project/views/homepage.dart';

class App extends StatelessWidget {
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "My Coffee House",
      theme: ThemeData(
        primaryColor:  Color.fromARGB(255, 16, 176, 163),
        colorScheme: ColorScheme.fromSwatch()
            .copyWith(secondary: Color.fromARGB(255, 16, 176, 163)),
      ),
      routes: {
        '/': (context) => MyHomePage(),
      },
      initialRoute: '/',
    );
  }
}

